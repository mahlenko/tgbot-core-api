<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendMessage;
use tgbot\CoreAPI\Telegram\Types\Keyboards\ReplyKeyboardRemove;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendMessage([
        'chat_id' => getenv('CHAT_ID'),
        'text' => 'Keyboard remove.',
        'reply_markup' => new ReplyKeyboardRemove()
    ])
));
