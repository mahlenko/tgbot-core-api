<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendMessage;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardButton;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

$keyboard = [
    'inline_keyboard' => [
        [
            new InlineKeyboardButton(['text' => 'Btn 1', 'callback_data' => 'data 1']),
            ['text' => 'Btn 2', 'callback_data' => 'data 2'],
            ['text' => 'Btn 3', 'callback_data' => 'data 3'],
        ],
        [
            ['text' => 'Btn 4', 'callback_data' => 'data 4']
        ]
    ]
];

dump($client->run(
    new SendMessage([
        'chat_id' => getenv('CHAT_ID'),
        'text' => 'Test message with inline keyboard.',
        'reply_markup' => $keyboard
    ])
));
