<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Set\SetChatTitle;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SetChatTitle([
        'chat_id' => getenv('CHAT_ID'),
        'title' => 'New title set'
    ])
));
