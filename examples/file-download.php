<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Get\GetFile;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

/* get file */
$file_id = '';
$get_file = $client->run( new GetFile(['file_id' => $file_id]) );

if ($get_file->ok)
{
    $download_url = $client->downloadFileUrl($get_file->result);

    // download
    file_put_contents(
        basename($download_url),
        file_get_contents($download_url)
    );
} else {
    dd($get_file);
}