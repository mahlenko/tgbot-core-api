<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendDocument;
use tgbot\CoreAPI\Telegram\Types\InputFile;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendDocument([
        'chat_id' => getenv('CHAT_ID'),
        'document' => new InputFile('documents/test.png'),
        'caption' => 'Document caption'
    ])
));
