<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendMessage;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendMessage([
        'chat_id' => getenv('CHAT_ID'),
        'text' => 'Test message with keyboard',
        'reply_markup' => [ // keyboard
            'resize_keyboard' => true,
            'keyboard' => [
                [ ['text' => '👨🏼‍💼 Profile'], ['text' => '🛎 Notification'] ],
                [ ['text' => '🤖 About'] ]
            ]
        ]
    ])
));
