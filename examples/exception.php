<?php

require '../vendor/autoload.php';

try {
    throw new Exception('Test exception', 400);
} catch (Exception $exception) {
    header('Content-type: application/json');
    dd(new \tgbot\CoreAPI\ResponseMessage([], $exception));
}

