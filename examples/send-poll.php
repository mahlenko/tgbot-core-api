<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendPoll;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendPoll([
        'chat_id' => getenv('CHAT_ID'),
        'question' => 'What year is it now?',
        'options' => range(2018, 2023)
    ])
));