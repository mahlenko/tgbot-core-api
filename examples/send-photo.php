<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendPhoto;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendPhoto([
        'chat_id' => getenv('CHAT_ID'),
        'caption' => 'Image caption',
        'photo' => 'documents/test.png'
    ])
));
