<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Send\SendMediaGroup;
use tgbot\CoreAPI\Telegram\Types\Media\Photo;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new SendMediaGroup([
        'chat_id' => getenv('CHAT_ID'),
        'media' => [
            new Photo(['media' => 'https://yastatic.net/s3/frontend/ydo/_/e51f85b8390acee1384b9038aa749c93.jpg']),
            new Photo(['media' => 'documents/test.png', 'caption' => 'Test caption']),
            new Photo(['media' => 'documents/test2.jpg']),
        ]
    ])
));