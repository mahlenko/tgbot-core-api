<?php

use tgbot\CoreAPI\BotClient;
use tgbot\CoreAPI\Telegram\Methods\Edit\EditMessageMedia;
use tgbot\CoreAPI\Telegram\Types\Media\Photo;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);

dump($client->run(
    new EditMessageMedia([
        'chat_id' => getenv('CHAT_ID'),
        'message_id' => 612,
        'media' => new Photo(['media' => 'documents/test2.jpg'])
    ])
) );

