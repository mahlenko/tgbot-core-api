<?php

use tgbot\CoreAPI\BotClient;

include __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new BotClient($_ENV['BOT_TOKEN']);
$update = $client->webhook();

dump($update);
