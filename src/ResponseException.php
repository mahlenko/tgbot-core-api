<?php


namespace tgbot\CoreAPI;


use Exception;

class ResponseException
{
    /**
     * @var string
     */
    public string $message;

    /**
     * @var int|mixed
     */
    public $code;

    /**
     * @var string
     */
    public string $file;

    /**
     * @var int
     */
    public int $line;

    /**
     * ResponseException constructor.
     * @param Exception $exception
     */
    public function __construct(Exception $exception)
    {
        $this->message = $exception->getMessage();
        $this->code = $exception->getCode();
        $this->file = $exception->getFile();
        $this->line = $exception->getLine();
    }
}