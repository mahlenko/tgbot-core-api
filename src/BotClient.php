<?php


namespace tgbot\CoreAPI;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Methods\Get\GetUpdates;
use tgbot\CoreAPI\Telegram\Types\Chat;
use tgbot\CoreAPI\Telegram\Types\File;
use tgbot\CoreAPI\Telegram\Types\Update;
use tgbot\CoreAPI\Telegram\Types\User;

class BotClient
{
    /**
     * @var string
     */
    private string $token;

    /**
     * @var string
     */
    private string $api_url = 'https://api.telegram.org/';

    /**
     * @var Client
     */
    public Client $client;

    /**
     * @var string
     */
    public string $type;

    /**
     * @var Chat
     */
    public Chat $chat;

    /**
     * @var Chat
     */
    public Chat $sender_chat;

    /**
     * @var User
     */
    public User $from;

    /**
     * @var Update
     */
    public Update $update;

    /**
     * BotClient constructor.
     * @param string $token
     * @param array $client_options
     */
    public function __construct(string $token, array $client_options = [])
    {
        $this->token = $token;
        $this->client = new Client($client_options);
    }

    /**
     * Get webhook data
     */
    public function webhook(): Update
    {
        /* Get updates */
        $this->update = new Update(
            json_decode(file_get_contents('php://input'))
        );

        $this->populateDataByUpdate();
        return $this->update;
    }

    /**
     * Run method request to Telegram API
     * @param TelegramMethodsAbstract $method
     * @return ResponseMessage
     * @throws GuzzleException
     */
    public function run(TelegramMethodsAbstract $method): ResponseMessage
    {
        /* */
        try {
            $method->requireFields();
        } catch (Exception $e) {
            return new ResponseMessage([
                'ok' => false,
                'description' => $e->getMessage()
            ]);
        }

        /* */
        $method->beforeSending();

        /* */
        $query = $this->getParamsQuery($method);

        /* get method name */
        $method_classname_segments = explode('\\', get_class($method));
        $method_name = $method_classname_segments[count($method_classname_segments) - 1];
        $request_url = $this->compareApiMethod() .'/'. $method_name;

        $telegram_request = new TelegramRequest([
            'method' => $method_name,
            'token' => $this->token,
            'url' => $request_url,
            'params' => $query
        ]);

        /* send telegram api */
        try {
            $response = $this->client->post($request_url, $query);
        } catch (RequestException $e) {
            return new ResponseMessage(json_decode( // error from the Telegram
                $e->getResponse()->getBody()->getContents()
            ), null, $telegram_request);
        }

        if ($response->getStatusCode() !== 200) {
            return new ResponseMessage([
                'ok' => false,
                'description' => 'Get status code: ' . $response->getStatusCode()
            ], null, $telegram_request);
        }

        try {
            $telegram_request->response = $response->getBody()->getContents();
            $content = json_decode($telegram_request->response);

            if (json_last_error()) {
                return new ResponseMessage([
                    'ok' => false,
                    'description' => json_last_error_msg()
                ], null, $telegram_request);
            }

            if ($content->ok) {
                $content->result = $method->bindToObject((array) $content->result);

                if ($method instanceof GetUpdates && $content->result) {
                    $this->update = $content->result[count($content->result) - 1];
                    $this->populateDataByUpdate();
                }
            }

            return new ResponseMessage($content, null, $telegram_request);
        } catch (Exception $e) {
            return new ResponseMessage([
                'ok' => false,
                'description' => $e->getMessage()
            ], $e, $telegram_request);
        }
    }

    /**
     * Download file url
     * @param File $file
     * @return string
     */
    public function downloadFileUrl(File $file): string
    {
        return rtrim($this->api_url, '/') .'/file/bot'. $this->token .'/'. $file->file_path;
    }

    /**
     * Returns the method url to the Telegram API
     * @return string
     */
    protected function compareApiMethod(): string
    {
        return rtrim($this->api_url, '/') .'/bot'. $this->token;
    }

    /**
     * Collects method parameters for the request
     * @param TelegramMethodsAbstract $method
     * @return array[]
     */
    protected function getParamsQuery(TelegramMethodsAbstract $method): array
    {
        $query_params = [];

        foreach (array_keys( get_class_vars(get_class($method)) ) as $key)
        {
            if (!empty($method->$key))
            {
                $query_params[$key] = $method->$key;
            }
        }

        return $method->hasFiles()
            ? ['multipart' => $this->multipart($query_params, $method->hasFiles())]
            : ['form_params' => $query_params];
    }

    /**
     * Collect multipart request
     * @param array $query
     * @param array $files
     * @return array
     */
    protected function multipart(array $query, array $files = []): array
    {
        $query_params = [];
        foreach ($query as $key => $value) {
            if (is_array($value)) {
                $value = json_encode($value);
            }

            $query_params[] = [
                'name' => $key,
                'contents' => $value
            ];
        }

        return array_merge($query_params, $files);
    }

    /**
     * Set settings from the Update
     */
    protected function populateDataByUpdate()
    {
        if (!isset($this->update)) return;

        foreach (get_class_vars(Update::class) as $type => $data) {
            if ($type == 'update_id' || !isset($this->update->$type)) continue;

            $update_type = $this->update->$type;
            $this->type = $type;

            if (isset($update_type->chat) || isset($update_type->message->chat)) {
                $this->chat = $update_type->chat ?? $update_type->message->chat;
            }

            if (isset($update_type->from)) {
                $this->from = $update_type->from;
            }

            if (isset($update_type->sender_chat) || isset($update_type->message->sender_chat)) {
                $this->sender_chat = $update_type->sender_chat ?? $update_type->message->sender_chat;
            }
        }
    }
}