<?php


namespace tgbot\CoreAPI;

use Exception;
use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Class ResponseMessage
 * @package tgbot\CoreAPI
 */
class ResponseMessage extends TelegramTypesAbstract
{
    /**
     * @var bool
     */
    public bool $ok = false;

    /**
     * @var mixed
     */
    public $result;

    /**
     * @var int
     */
    public int $error_code;

    /**
     * @var string
     */
    public string $description;

    /**
     * @var ResponseException
     */
    public ResponseException $exception;

    /**
     * @var TelegramRequest
     */
    public TelegramRequest $debug;

    /**
     * ResponseMessage constructor.
     * @param array $data
     * @param Exception|null $exception
     * @param TelegramRequest|null $request
     */
    public function __construct($data = [], Exception $exception = null, TelegramRequest $request = null)
    {
        parent::__construct($data);

        $debug = true;
        if (key_exists('APP_DEBUG', $_ENV)) {
            $debug = in_array((string)$_ENV['APP_DEBUG'], ['1', 'true']);
        }

        if ($debug) {
            if ($exception) {
                $this->exception = new ResponseException($exception);
            }

            if ($request) {
                $this->debug = $request;
            }
        }
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}