<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\WebApp\SentWebAppMessage;

/**
 * Use this method to set the result of an interaction with a Web App
 * and send a corresponding message on behalf of the user to the chat from which
 * the query originated. On success, a SentWebAppMessage object is returned.
 * @see https://core.telegram.org/bots/api#answerwebappquery
 */
class AnswerWebAppQuery extends TelegramMethodsAbstract
{
    /**
     * Optional. Identifier of the sent inline message.
     * Available only if there is an inline keyboard attached to the message.
     * @var string
     */
    public string $web_app_query_id = '';

    public InlineQueryResult $result;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function requiredFields(): array
    {
        return ['web_app_query_id', 'result'];
    }

    public function bindToObject($data)
    {
        return new SentWebAppMessage($data);
    }

    public function beforeSending()
    {
        //...
    }
}