<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to kick a user from a group, a supergroup or a channel.
 * In the case of supergroups and channels, the user will not be able to return
 * to the group on their own using invite links, etc., unless unbanned first.
 * The bot must be an administrator in the chat for this to work and must have
 * the appropriate admin rights. Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class KickChatMember extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target group or username of the target
     * supergroup or channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Unique identifier of the target user
     * @var int
     */
    public int $user_id = 0;

    /**
     * Date when the user will be unbanned, unix time. If user is banned for
     * more than 366 days or less than 30 seconds from the current time they
     * are considered to be banned forever
     * @var int
     */
    public int $until_date = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}