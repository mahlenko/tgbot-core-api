<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to add a new sticker to a set created by the bot. Returns True on success.
 * @see https://core.telegram.org/bots/api#addstickertoset
 */
class AddStickerToSet extends TelegramMethodsAbstract
{
    /**
     * User identifier of sticker set owner
     * @var int
     */
    public int $user_id = 0;

    /**
     * Sticker set name
     * @var string
     */
    public string $name = '';

    /**
     * Png image with the sticker, must be up to 512 kilobytes in size,
     * dimensions must not exceed 512px, and either width or height must
     * be exactly 512px. Pass a file_id as a String to send a file that
     * already exists on the Telegram servers, pass an HTTP URL as a String
     * for Telegram to get a file from the Internet, or upload a new one
     * using multipart/form-data.
     * @var string
     */
    public string $png_sticker;

    /**
     * TGS animation with the sticker, uploaded using multipart/form-data.
     * See https://core.telegram.org/animated_stickers#technical-requirements
     * for technical requirements
     * @var string
     */
    public string $tgs_sticker;

    /**
     * One or more emoji corresponding to the sticker
     * @var string
     */
    public string $emojis = '';

    /**
     * A JSON-serialized object for position where the mask should be placed on faces
     * @var string
     */
    public string $mask_position;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'name', 'png_sticker', 'emojis'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->png_sticker = $this->attachFile($this->png_sticker);
        $this->tgs_sticker = $this->attachFile($this->tgs_sticker);
    }
}