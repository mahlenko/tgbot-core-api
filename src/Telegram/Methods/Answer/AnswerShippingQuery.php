<?php

namespace tgbot\CoreAPI\Telegram\Methods\Answer;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Payments\ShippingOption;

/**
 * If you sent an invoice requesting a shipping address and the parameter
 * is_flexible was specified, the Bot API will send an Update with a shipping_query
 * field to the bot. Use this method to reply to shipping queries. On success,
 * True is returned.
 * @see https://core.telegram.org/bots/api#answershippingquery
 */
class AnswerShippingQuery extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the query to be answered
     * @var string
     */
    public string $shipping_query_id = '';

    /**
     * Specify True if delivery to the specified address is possible and False
     * if there are any problems (for example, if delivery to the specified
     * address is not possible)
     * @var bool
     */
    public bool $ok = false;

    /**
     * Required if ok is True. A JSON-serialized array of available shipping options.
     * @var ShippingOption[]
     */
    public array $shipping_options = [];

    /**
     * Required if ok is False. Error message in human readable form that explains
     * why it is impossible to complete the order (e.g. "Sorry, delivery to
     * your desired address is unavailable'). Telegram will display this message
     * to the user.
     * @var string
     */
    public string $error_message = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['shipping_query_id', 'ok'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return null;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}