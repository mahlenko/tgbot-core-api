<?php

namespace tgbot\CoreAPI\Telegram\Methods\Answer;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to send answers to callback queries sent from inline keyboards.
 * The answer will be displayed to the user as a notification at the top of the
 * chat screen or as an alert. On success, True is returned.
 *
 * | Alternatively, the user can be redirected to the specified Game URL.
 * | For this option to work, you must first create a game for your bot via
 * | @Botfather and accept the terms. Otherwise, you may use links like
 * | t.me/your_bot?start=XXXX that open your bot with a parameter.
 *
 * @package tgbot\CoreAPI\Methods
 */
class AnswerCallbackQuery extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the query to be answered
     * @var string
     */
    public string $callback_query_id = '';

    /**
     * Text of the notification. If not specified, nothing will be shown
     * to the user, 0-200 characters
     * @var string
     */
    public string $text = '';

    /**
     * If true, an alert will be shown by the client instead of a notification
     * at the top of the chat screen. Defaults to false.
     * @var bool
     */
    public bool $show_alert = false;

    /**
     * URL that will be opened by the user's client. If you have created a Game
     * and accepted the conditions via @Botfather, specify the URL that opens
     * your game — note that this will only work if the query comes from a
     * callback_game button.
     *
     * | Otherwise, you may use links like t.me/your_bot?start=XXXX that open
     * | your bot with a parameter.
     *
     * @var string
     */
    public string $url = '';

    /**
     * The maximum amount of time in seconds that the result of the callback
     * query may be cached client-side. Telegram apps will support caching
     * starting in version 3.14. Defaults to 0.
     * @var int
     */
    public int $cache_time = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['callback_query_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
