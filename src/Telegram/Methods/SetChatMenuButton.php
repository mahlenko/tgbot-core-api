<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Menu\MenuButton;
use tgbot\CoreAPI\Telegram\Types\WebApp\SentWebAppMessage;

/**
 * Use this method to change the bot's menu button in a private chat,
 * or the default menu button. Returns True on success.
 * @see https://core.telegram.org/bots/api#setchatmenubutton
 */
class SetChatMenuButton extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target private chat.
     * If not specified, default bot's menu button will be changed.
     * @var int
     */
    public int $chat_id;

    /**
     * A JSON-serialized object for the bot's new menu button.
     * Defaults to MenuButtonDefault
     * @var MenuButton|string
     */
    public $menu_button;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function requiredFields(): array
    {
        return [];
    }

    public function bindToObject($data)
    {
        return $data;
    }

    public function beforeSending()
    {
        if (!empty($this->menu_button)) {
            $this->menu_button = json_encode(array_filter((array) $this->menu_button));
        }
    }
}