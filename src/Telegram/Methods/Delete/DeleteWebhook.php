<?php

namespace tgbot\CoreAPI\Telegram\Methods\Delete;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to remove webhook integration if you decide to switch back
 * to getUpdates. Returns True on success. Requires no parameters.
 * @package tgbot\CoreAPI\Methods
 */
class DeleteWebhook extends TelegramMethodsAbstract
{
    /**
     * @inheritDoc
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}