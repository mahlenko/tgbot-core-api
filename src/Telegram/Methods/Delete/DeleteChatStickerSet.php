<?php

namespace tgbot\CoreAPI\Telegram\Methods\Delete;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to delete a group sticker set from a supergroup.
 * The bot must be an administrator in the chat for this to work and must have
 * the appropriate admin rights. Use the field can_set_sticker_set optionally
 * returned in getChat requests to check if the bot can use this method.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class DeleteChatStickerSet extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * (in the format @supergroupusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
