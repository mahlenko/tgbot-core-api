<?php

namespace tgbot\CoreAPI\Telegram\Methods\Delete;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to delete a sticker from a set created by the bot. Returns True on success.
 * @see https://core.telegram.org/bots/api#deletestickerfromset
 */
class DeleteStickerFromSet extends TelegramMethodsAbstract
{
    /**
     * File identifier of the sticker
     * @var string
     */
    public string $sticker = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['sticker'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}