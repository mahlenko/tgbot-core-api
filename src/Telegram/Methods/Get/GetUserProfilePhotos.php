<?php

namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\UserProfilePhotos;

/**
 * Use this method to get a list of profile pictures for a user. Returns a
 * UserProfilePhotos object.
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#getuserprofilephotos
 */
class GetUserProfilePhotos extends TelegramMethodsAbstract
{
    /**
     * Unique identifier of the target user
     * @var string
     */
    public string $user_id = '';

    /**
     * Sequential number of the first photo to be returned. By default, all
     * photos are returned.
     * @var int
     */
    public int $offset = 0;

    /**
     * Limits the number of photos to be retrieved. Values between 1—100 are accepted. Defaults to 100.
     * @var int
     */
    public int $limit = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new UserProfilePhotos($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}