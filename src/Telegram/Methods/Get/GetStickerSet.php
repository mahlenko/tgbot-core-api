<?php

namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Stickers\StickerSet;

/**
 * Use this method to get a sticker set. On success, a StickerSet object is returned.
 * @see https://core.telegram.org/bots/api#getstickerset
 */
class GetStickerSet extends TelegramMethodsAbstract
{
    /**
     * Name of the sticker set
     * @var string
     */
    public string $name = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['name'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new StickerSet($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}