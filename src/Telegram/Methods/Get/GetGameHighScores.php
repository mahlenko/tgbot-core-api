<?php

namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\GameHighScore;

/**
 * Use this method to get data for high score tables. Will return the score
 * of the specified user and several of his neighbors in a game. On success,
 * returns an Array of GameHighScore objects.
 *
 * | This method will currently return scores for the target user, plus two
 * | of his closest neighbors on each side. Will also return the top three users
 * | if the user and his neighbors are not among them. Please note that this
 * | behavior is subject to change.
 *
 * @see https://core.telegram.org/bots/api#getgamehighscores
 */
class GetGameHighScores extends TelegramMethodsAbstract
{
    /**
     * Target user id
     * @var int
     */
    public int $user_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Unique identifier for the target chat
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Identifier of the sent message
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $this->bindToObjectArray(GameHighScore::class, $data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}