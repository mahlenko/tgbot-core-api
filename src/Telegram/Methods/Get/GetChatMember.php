<?php

namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\ChatMember;

/**
 * Use this method to get information about a member of a chat.
 * Returns a ChatMember object on success.
 * @package tgbot\CoreAPI\Methods
 */
class GetChatMember extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * or channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Unique identifier of the target user
     * @var int
     */
    public int $user_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new ChatMember($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
