<?php

namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\File;

/**
 * Use this method to get basic info about a file and prepare it for downloading.
 * For the moment, bots can download files of up to 20MB in size. On success,
 * a File object is returned. The file can then be downloaded via the link
 * https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is
 * taken from the response. It is guaranteed that the link will be valid for
 * at least 1 hour. When the link expires, a new one can be requested by
 * calling getFile again.
 *
 * | Note: This function may not preserve the original file name and MIME type.
 * | You should save the file's MIME type and name (if available) when the File
 * | object is received.
 *
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#getfile
 */
class GetFile extends TelegramMethodsAbstract
{
    /**
     * File identifier to get info about
     * @var string
     */
    public string $file_id = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['file_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new File($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}