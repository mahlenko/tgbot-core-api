<?php


namespace tgbot\CoreAPI\Telegram\Methods\Get;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\WebhookInfo;

/**
 * Use this method to get current webhook status. Requires no parameters.
 * On success, returns a WebhookInfo object. If the bot is using getUpdates,
 * will return an object with the url field empty.
 * @package tgbot\CoreAPI\Methods
 */
class GetWebhookInfo extends TelegramMethodsAbstract
{
    /**
     * @inheritDoc
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function bindToObject($data)
    {
        return new WebhookInfo($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}