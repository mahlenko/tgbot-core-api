<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to set the score of the specified user in a game.
 * On success, if the message was sent by the bot, returns the edited Message,
 * otherwise returns True. Returns an error, if the new score is not greater
 * than the user's current score in the chat and force is False.
 * @see https://core.telegram.org/bots/api#setgamescore
 */
class SetGameScore extends TelegramMethodsAbstract
{
    /**
     * User identifier
     * @var int
     */
    public int $user_id = 0;

    /**
     * New score, must be non-negative
     * @var int
     */
    public int $score = 0;

    /**
     * Pass True, if the high score is allowed to decrease.
     * This can be useful when fixing mistakes or banning cheaters
     * @var bool
     */
    public bool $force = false;

    /**
     * Pass True, if the game message should not be automatically edited to
     * include the current scoreboard
     * @var bool
     */
    public bool $disable_edit_message = false;

    /**
     * Required if inline_message_id is not specified. Unique identifier
     * for the target chat
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var int
     */
    public int $inline_message_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'score'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}