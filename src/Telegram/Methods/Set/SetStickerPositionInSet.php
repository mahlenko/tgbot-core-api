<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to move a sticker in a set created by the bot to
 * a specific position. Returns True on success.
 * @see https://core.telegram.org/bots/api#setstickerpositioninset
 */
class SetStickerPositionInSet extends TelegramMethodsAbstract
{
    /**
     * File identifier of the sticker
     * @var string
     */
    public string $sticker = '';

    /**
     * New sticker position in the set, zero-based
     * @var int
     */
    public int $position = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['sticker', 'position'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}