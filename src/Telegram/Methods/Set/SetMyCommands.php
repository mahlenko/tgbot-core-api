<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\BotCommand;

/**
 * Use this method to change the list of the bot's commands.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class SetMyCommands extends TelegramMethodsAbstract
{
    /**
     * A JSON-serialized list of bot commands to be set as the list of the bot's commands.
     * At most 100 commands can be specified.
     * @var array|BotCommand
     */
    public $commands = [];

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['commands'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
