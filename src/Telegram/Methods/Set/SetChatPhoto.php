<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to set a new profile photo for the chat.
 * Photos can't be changed for private chats. The bot must be an administrator
 * in the chat for this to work and must have the appropriate admin rights.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class SetChatPhoto extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * New chat photo, uploaded using multipart/form-data
     * @var string
     */
    public string $photo;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'photo'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->photo = $this->attachFile($this->photo);
    }
}

