<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\ChatPermissions;

/**
 * Use this method to set default chat permissions for all members.
 * The bot must be an administrator in the group or a supergroup for
 * this to work and must have the can_restrict_members admin rights.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class SetChatPermissions extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target
     * supergroup (in the format @supergroupusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * New default chat permissions
     * @var ChatPermissions
     */
    public ChatPermissions $permissions;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'permissions'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
