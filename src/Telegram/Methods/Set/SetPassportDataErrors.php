<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Informs a user that some of the Telegram Passport elements they provided
 * contains errors. The user will not be able to re-submit their Passport to you
 * until the errors are fixed (the contents of the field for which you returned
 * the error must change). Returns True on success.
 *
 * Use this if the data submitted by the user doesn't satisfy the standards your
 * service requires for any reason. For example, if a birthday date seems invalid,
 * a submitted document is blurry, a scan shows evidence of tampering, etc. Supply
 * some details in the error message to make sure the user knows how to correct
 * the issues.
 * @see https://core.telegram.org/bots/api#setpassportdataerrors
 */
class SetPassportDataErrors extends TelegramMethodsAbstract
{
    /**
     * User identifier
     * @var int
     */
    public int $user_id = 0;

    /**
     * A JSON-serialized array describing the errors
     * @var array
     */
    public array $errors = [];

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'errors'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}