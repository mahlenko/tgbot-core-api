<?php

namespace tgbot\CoreAPI\Telegram\Methods\Set;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to set a new group sticker set for a supergroup.
 * The bot must be an administrator in the chat for this to work and must have
 * the appropriate admin rights. Use the field can_set_sticker_set optionally
 * returned in getChat requests to check if the bot can use this method.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class SetChatStickerSet extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * (in the format @supergroupusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Name of the sticker set to be set as the group sticker set
     * @var string
     */
    public string $sticker_set_name = '';

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'sticker_set_name'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
