<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Menu\MenuButton;
use tgbot\CoreAPI\Telegram\Types\WebApp\SentWebAppMessage;

/**
 * Use this method to get the current value of the bot's menu button in a private chat,
 * or the default menu button. Returns MenuButton on success.
 * @see https://core.telegram.org/bots/api#getchatmenubutton
 */
class GetChatMenuButton extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target private chat.
     * If not specified, default bot's menu button will be returned
     * @var int
     */
    public int $chat_id;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function requiredFields(): array
    {
        return [];
    }

    public function bindToObject($data)
    {
        return new MenuButton($data);
    }

    public function beforeSending()
    {
        //...
    }
}