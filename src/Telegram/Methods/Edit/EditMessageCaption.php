<?php

namespace tgbot\CoreAPI\Telegram\Methods\Edit;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to edit captions of messages. On success, if edited message
 * is sent by the bot, the edited Message is returned, otherwise True is returned.
 * @package tgbot\CoreAPI\Methods
 */
class EditMessageCaption extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Identifier of the message to edit
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * New caption of the message, 0-1024 characters after entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}
