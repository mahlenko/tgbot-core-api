<?php

namespace tgbot\CoreAPI\Telegram\Methods\Edit;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\InputMedia;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to edit animation, audio, document, photo, or video messages.
 * If a message is a part of a message album, then it can be edited only to a photo or a video.
 * Otherwise, message type can be changed arbitrarily. When inline message is edited,
 * new file can't be uploaded. Use previously uploaded file via its file_id or specify a URL.
 * On success, if the edited message was sent by the bot, the edited Message is returned,
 * otherwise True is returned.
 * @package tgbot\CoreAPI\Methods
 */
class EditMessageMedia extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified.
     * Identifier of the message to edit
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * A JSON-serialized object for a new media content of the message
     * @var InputMedia
     */
    public InputMedia $media;

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['media'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->media->media = $this->attachFile($this->media->media);
        $this->media = json_encode($this->media);

        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }

}
