<?php

namespace tgbot\CoreAPI\Telegram\Methods\Edit;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to edit text and game messages. On success, if edited message
 * is sent by the bot, the edited Message is returned, otherwise True is returned.
 * @package tgbot\CoreAPI\Methods
 */
class EditMessageText extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message to edit
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified.
     * Identifier of the inline message
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * New text of the message, 1-4096 characters after entities parsing
     * @var string
     */
    public string $text = '';

    /**
     * Mode for parsing entities in the message text.
     * See formatting options for more details.
     * @var string
     */
    public string $parse_mode = '';

    /**
     * Disables link previews for links in this message
     * @var bool
     */
    public bool $disable_web_page_preview = false;

    /**
     * A JSON-serialized object for an inline keyboard.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['text'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}
