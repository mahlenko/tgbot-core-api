<?php


namespace tgbot\CoreAPI\Telegram\Methods\Edit;


use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to edit live location messages. A location can be edited until
 * its live_period expires or editing is explicitly disabled by a call to
 * stopMessageLiveLocation. On success, if the edited message was sent by the bot,
 * the edited Message is returned, otherwise True is returned.
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#editmessagelivelocation
 */
class EditMessageLiveLocation extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message to edit
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     * @var int
     */
    public int $inline_message_id = 0;

    /**
     * Latitude of new location
     * @var float
     */
    public float $latitude = 0.00;

    /**
     * Longitude of new location
     * @var float
     */
    public float $longitude = 0.00;

    /**
     * Optional. The radius of uncertainty for the location, measured in meters; 0-1500
     * @var float
     */
    public float $horizontal_accuracy = 0.0;

    /**
     * Optional. For live locations, a direction in which the user is moving, in degrees.
     * Must be between 1 and 360 if specified.
     * @var int
     */
    public int $heading;

    /**
     * Optional. For live locations, a maximum distance for proximity alerts about
     * approaching another chat member, in meters. Must be between 1 and 100000 if specified.
     * @var int
     */
    public int $proximity_alert_radius;

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'latitude', 'longitude'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}