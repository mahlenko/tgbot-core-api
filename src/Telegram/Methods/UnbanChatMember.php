<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to unban a previously kicked user in a supergroup or channel.
 * The user will not return to the group or channel automatically, but will be
 * able to join via link, etc. The bot must be an administrator for this to work.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class UnbanChatMember extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target group or username of the target
     * supergroup or channel (in the format @username)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Unique identifier of the target user
     * @var int
     */
    public int $user_id = 0;

    /**
     * Do nothing if the user is not banned
     * @var bool
     */
    public bool $only_if_banned;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'user_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}