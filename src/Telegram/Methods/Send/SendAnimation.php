<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to send animation files (GIF or H.264/MPEG-4 AVC video
 * sound). On success, the sent Message is returned. Bots can currently send
 * animation files of up to 50 MB in size, this limit may be changed in the
 * future.
 * @see https://core.telegram.org/bots/api#sendanimation
 */
class SendAnimation extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Animation to send. Pass a file_id as String to send an animation that exists
     * on the Telegram servers (recommended), pass an HTTP URL as a String for
     * Telegram to get an animation from the Internet, or upload a new animation
     * using multipart/form-data.
     * @var string
     */
    public string $animation;

    /**
     * Duration of sent animation in seconds
     * @var int
     */
    public int $duration = 0;

    /**
     * Animation width
     * @var int
     */
    public int $width = 0;

    /**
     * Animation height
     * @var int
     */
    public int $height = 0;

    /**
     * Thumbnail of the file sent; can be ignored if thumbnail generation for
     * the file is supported server-side. The thumbnail should be in JPEG format
     * and less than 200 kB in size. A thumbnail‘s width and height should not
     * exceed 320. Ignored if the file is not uploaded using multipart/form-data.
     * Thumbnails can’t be reused and can be only uploaded as a new file, so you
     * can pass “attach://<file_attach_name>” if the thumbnail was uploaded using
     * multipart/form-data under <file_attach_name>.
     * @var string
     */
    public string $thumb;

    /**
     * Document caption (may also be used when resending documents by file_id),
     * 0-1024 characters after entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Mode for parsing entities in the message text. See formatting options for more details.
     * @see https://core.telegram.org/bots/api#formatting-options
     * @var string
     */
    public string $parse_mode = '';

    /**
     * List of special entities that appear in the caption, which can be specified instead of parse_mode
     * @var array
     */
    public array $caption_entities = [];

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force a
     * reply from the user.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'animation'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->animation = $this->attachFile($this->animation);
        $this->thumb = $this->attachFile($this->thumb);

        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}