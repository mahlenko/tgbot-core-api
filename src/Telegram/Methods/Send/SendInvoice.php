<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;
use tgbot\CoreAPI\Telegram\Types\Payments\LabeledPrice;

/**
 * Use this method to send invoices. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendinvoice
 */
class SendInvoice extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target private chat
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Product name, 1-32 characters
     * @var string
     */
    public string $title = '';

    /**
     * Product description, 1-255 characters
     * @var string
     */
    public string $description = '';

    /**
     * Bot-defined invoice payload, 1-128 bytes. This will not be displayed to
     * the user, use for your internal processes.
     * @var string
     */
    public string $payload = '';

    /**
     * Payments provider token, obtained via Botfather
     * @var string
     */
    public string $provider_token = '';

    /**
     * Three-letter ISO 4217 currency code, see more on currencies
     * @var string
     */
    public string $currency = '';

    /**
     * Price breakdown, a JSON-serialized list of components (e.g. product price,
     * tax, discount, delivery cost, delivery tax, bonus, etc.)
     * @var LabeledPrice[]
     */
    public array $prices = [];

    /**
     * The maximum accepted amount for tips in the smallest units of the currency
     * (integer, not float/double). For example, for a maximum tip of US$ 1.45
     * pass max_tip_amount = 145. See the exp parameter in currencies.json,
     * it shows the number of digits past the decimal point for each currency
     * (2 for the majority of currencies). Defaults to 0
     * @see https://core.telegram.org/bots/payments/currencies.json
     * @var int
     */
    public int $max_tip_amount = 0;

    /**
     * A JSON-serialized array of suggested amounts of tips in the smallest
     * units of the currency (integer, not float/double). At most 4 suggested
     * tip amounts can be specified. The suggested tip amounts must be positive,
     * passed in a strictly increased order and must not exceed max_tip_amount.
     * @var array[int]
     */
    public array $suggested_tip_amounts = [];

    /**
     * Unique deep-linking parameter that can be used to generate
     * this invoice when used as a start parameter
     * @var string
     */
    public string $start_parameter = '';

    /**
     * JSON-encoded data about the invoice, which will be shared with
     * the payment provider. A detailed description of required fields should
     * be provided by the payment provider.
     * @var string
     */
    public string $provider_data = '';

    /**
     * URL of the product photo for the invoice. Can be a photo of the goods or
     * a marketing image for a service. People like it better when they see what
     * they are paying for.
     * @var string
     */
    public string $photo_url = '';

    /**
     * Photo size
     * @var int
     */
    public int $photo_size = 0;

    /**
     * Photo width
     * @var int
     */
    public int $photo_width = 0;

    /**
     * Photo height
     * @var int
     */
    public int $photo_height = 0;

    /**
     * Pass True, if you require the user's full name to complete the order
     * @var bool
     */
    public bool $need_name = false;

    /**
     * Pass True, if you require the user's phone number to complete the order
     * @var bool
     */
    public bool $need_phone_number = false;

    /**
     * Pass True, if you require the user's email address to complete the order
     * @var bool
     */
    public bool $need_email = false;

    /**
     * Pass True, if you require the user's shipping address to complete the order
     * @var bool
     */
    public bool $need_shipping_address = false;

    /**
     * Pass True, if user's phone number should be sent to provider
     * @var bool
     */
    public bool $send_phone_number_to_provider = false;

    /**
     * Pass True, if user's email address should be sent to provider
     * @var bool
     */
    public bool $send_email_to_provider = false;

    /**
     * Pass True, if the final price depends on the shipping method
     * @var bool
     */
    public bool $is_flexible = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * A JSON-serialized object for an inline keyboard. If empty,
     * one 'Pay total price' button will be shown. If not empty, the first
     * button must be a Pay button.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'title', 'description', 'payload', 'provider_token', 'currency', 'prices'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}