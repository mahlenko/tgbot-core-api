<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;
use tgbot\CoreAPI\Telegram\Types\MessageEntity;

/**
 * Use this method to send a native poll. On success, the sent Message is returned.
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#sendpoll
 */
class SendPoll extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Poll question, 1-255 characters
     * @var string
     */
    public string $question = '';

    /**
     * A JSON-serialized list of answer options, 2-10 strings 1-100 characters each
     * @var array|string
     */
    public $options = [];

    /**
     * True, if the poll needs to be anonymous, defaults to True
     * @var bool
     */
    public bool $is_anonymous = true;

    /**
     * Poll type, "quiz" or "regular", defaults to "regular"
     * @var string
     */
    public string $type = 'regular';

    /**
     * True, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to False
     * @var bool
     */
    public bool $allows_multiple_answers = false;

    /**
     * 0-based identifier of the correct answer option, required for polls in quiz mode
     * @var int
     */
    public int $correct_option_id = 0;

    /**
     * Text that is shown when a user chooses an incorrect answer or taps on the
     * lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds
     * after entities parsing
     * @var string
     */
    public string $explanation = '';

    /**
     * Mode for parsing entities in the explanation. See formatting options for more details.
     * @var string
     */
    public string $explanation_parse_mode = '';

    /**
     * List of special entities that appear in the poll explanation, which can be specified instead of parse_mode
     * @var MessageEntity[]
     */
    public array $explanation_entities = [];

    /**
     * Amount of time in seconds the poll will be active after creation, 5-600.
     * Can't be used together with close_date.
     * @var int
     */
    public int $open_period = 0;

    /**
     * Point in time (Unix timestamp) when the poll will be automatically closed.
     * Must be at least 5 and no more than 600 seconds in the future.
     * Can't be used together with open_period.
     * @var int
     */
    public int $close_date = 0;

    /**
     * Pass True, if the poll needs to be immediately closed. This can be useful for poll preview.
     * @var bool
     */
    public bool $is_closed = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Additional interface options. A JSON-serialized object for an inline
     * keyboard, custom reply keyboard, instructions to remove reply keyboard
     * or to force a reply from the user.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'question', 'options'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!is_string($this->options))
        {
            foreach ($this->options as $key => $val) {
                if (is_numeric($val)) {
                    $this->options[$key] = (string) $val;
                }
            }

            $this->options = json_encode($this->options);
        }

        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}