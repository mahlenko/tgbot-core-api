<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to send a group of photos or videos as an album.
 * On success, an array of the sent Messages is returned.
 * @see https://core.telegram.org/bots/api#sendmediagroup
 */
class SendMediaGroup extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * A JSON-serialized array describing photos and videos to be sent,
     * must include 2–10 items
     * @var array
     */
    public array $media = [];

    /**
     * Sends the messages silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the messages are a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = true;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'media'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $this->bindToObjectArray(Message::class, $data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        foreach ($this->media as $index => $file) {
            $this->media[$index]->media = $this->attachFile($file->media);
        }
    }
}