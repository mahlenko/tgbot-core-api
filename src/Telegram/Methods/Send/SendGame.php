<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to send a game. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendgame
 */
class SendGame extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat
     * @var int
     */
    public int $chat_id = 0;

    /**
     * @var string
     */
    public string $game_short_name = '';

    /**
     * Short name of the game, serves as the unique identifier for the game.
     * Set up your games via Botfather.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * If the message is a reply, ID of the original message
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'game_short_name'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}