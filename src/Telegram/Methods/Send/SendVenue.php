<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to send information about a venue. On success, the sent
 * Message is returned.
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#sendvenue
 */
class SendVenue extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Latitude of the venue
     * @var float
     */
    public float $latitude = 0.00;

    /**
     * Longitude of the venue
     * @var float
     */
    public float $longitude = 0.00;

    /**
     * Name of the venue
     * @var string
     */
    public string $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public string $address = '';

    /**
     * Foursquare identifier of the venue
     * @var string
     */
    public string $foursquare_id = '';

    /**
     * Foursquare type of the venue, if known. (For example,
     * “arts_entertainment/default”, “arts_entertainment/aquarium”
     * or “food/icecream”.)
     * @var string
     */
    public string $foursquare_type = '';

    /**
     * Optional. Google Places identifier of the venue
     * @var string
     */
    public string $google_place_id = '';

    /**
     * Optional. Google Places type of the venue. (See supported types.)
     * @see https://developers.google.com/places/web-service/supported_types
     * @var string
     */
    public string $google_place_type = '';

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Additional interface options. A JSON-serialized object for an
     * inline keyboard, custom reply keyboard, instructions to remove
     * reply keyboard or to force a reply from the user.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'latitude', 'longitude', 'title', 'address'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}