<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\InputFile;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to send photos. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#sendphoto
 */
class SendPhoto extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Photo to send. Pass a file_id as String to send a photo that exists on
     * the Telegram servers (recommended), pass an HTTP URL as a String
     * for Telegram to get a photo from the Internet, or upload a new photo
     * using multipart/form-data.
     * @see https://core.telegram.org/bots/api#sending-files
     * @var InputFile|string
     */
    public $photo;

    /**
     * Photo caption (may also be used when resending photos by file_id),
     * 0-1024 characters after entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Mode for parsing entities in the message text. See formatting options for more details.
     * @see https://core.telegram.org/bots/api#formatting-options
     * @var string
     */
    public string $parse_mode = '';

    /**
     * List of special entities that appear in the caption, which can be specified instead of parse_mode
     * @var array
     */
    public array $caption_entities = [];

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force a
     * reply from the user.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'photo'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->photo = $this->attachFile($this->photo);

        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}