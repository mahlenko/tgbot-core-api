<?php

namespace tgbot\CoreAPI\Telegram\Methods\Send;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\Message;

class SendSticker extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Sticker to send. Pass a file_id as String to send a file that exists
     * on the Telegram servers (recommended), pass an HTTP URL as a String
     * for Telegram to get a .webp file from the Internet, or upload a new
     * one using multipart/form-data.
     * @var string
     */
    public string $sticker;

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard,
     * custom reply keyboard, instructions to remove reply keyboard or to force
     * a reply from the user.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'sticker'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->sticker = $this->attachFile($this->sticker);

        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}