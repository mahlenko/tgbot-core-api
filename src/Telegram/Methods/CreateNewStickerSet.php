<?php


namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Stickers\MaskPosition;

/**
 * Use this method to create new sticker set owned by a user.
 * The bot will be able to edit the created sticker set.
 * Returns True on success.
 * @see https://core.telegram.org/bots/api#createnewstickerset
 */
class CreateNewStickerSet extends TelegramMethodsAbstract
{
    /**
     * User identifier of created sticker set owner
     * @var int
     */
    public int $user_id = 0;

    /**
     * Short name of sticker set, to be used in t.me/addstickers/
     * URLs (e.g., animals). Can contain only english letters, digits and
     * underscores. Must begin with a letter, can't contain consecutive
     * underscores and must end in “_by_<bot username>”. <bot_username> is
     * case insensitive. 1-64 characters.
     * @var string
     */
    public string $name = '';

    /**
     * Sticker set title, 1-64 characters
     * @var string
     */
    public string $title = '';

    /**
     * Png image with the sticker, must be up to 512 kilobytes in size,
     * dimensions must not exceed 512px, and either width or height must be
     * exactly 512px. Pass a file_id as a String to send a file that already
     * exists on the Telegram servers, pass an HTTP URL as a String for Telegram
     * to get a file from the Internet, or upload a new one using multipart/form-data.
     * @var string
     */
    public string $png_sticker;

    /**
     * TGS animation with the sticker, uploaded using multipart/form-data.
     * See https://core.telegram.org/animated_stickers#technical-requirements
     * for technical requirements
     * @var string
     */
    public string $tgs_sticker;

    /**
     * One or more emoji corresponding to the sticker
     * @var string
     */
    public string $emojis = '';

    /**
     * Pass True, if a set of mask stickers should be created
     * @var bool
     */
    public bool $contains_masks = false;

    /**
     * A JSON-serialized object for position where the mask should be placed on faces
     * @var MaskPosition
     */
    public MaskPosition $mask_position;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'name', 'title', 'png_sticker', 'emojis'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->png_sticker = $this->attachFile($this->png_sticker);
        $this->tgs_sticker = $this->attachFile($this->tgs_sticker);
    }
}