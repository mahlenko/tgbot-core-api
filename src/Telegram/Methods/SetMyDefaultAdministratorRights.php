<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\ChatAdministratorRights;

/**
 * Use this method to change the default administrator rights requested by the bot
 * when it's added as an administrator to groups or channels. These rights
 * will be suggested to users, but they are are free to modify the list before adding the bot.
 * Returns True on success.
 * @see https://core.telegram.org/bots/api#setmydefaultadministratorrights
 */
class SetMyDefaultAdministratorRights extends TelegramMethodsAbstract
{
    /**
     * A JSON-serialized object describing new default administrator rights.
     * If not specified, the default administrator rights will be cleared.
     * @var ChatAdministratorRights
     */
    public ChatAdministratorRights $rights;

    /**
     * Pass True to change the default administrator rights of the bot in channels.
     * Otherwise, the default administrator rights of the bot for groups and supergroups will be changed.
     * @var bool
     */
    public bool $for_channels;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function requiredFields(): array
    {
        return [];
    }

    public function bindToObject($data)
    {
        return $data;
    }

    public function beforeSending()
    {
        //...
    }
}