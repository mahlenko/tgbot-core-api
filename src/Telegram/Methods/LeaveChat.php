<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method for your bot to leave a group, supergroup or channel.
 * Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class LeaveChat extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target
     * supergroup or channel (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}

