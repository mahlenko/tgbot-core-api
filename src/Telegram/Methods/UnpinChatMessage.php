<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to unpin a message in a group, a supergroup, or a channel.
 * The bot must be an administrator in the chat for this to work and must have the
 * 'can_pin_messages' admin right in the supergroup or 'can_edit_messages'
 * admin right in the channel. Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class UnpinChatMessage extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Identifier of a message to unpin. If not specified, the most recent pinned
     * message (by sending date) will be unpinned.
     * @var int
     */
    public int $message_id;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}
