<?php


namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to clear the list of pinned messages in a chat.
 * If the chat is not a private chat, the bot must be an administrator
 * in the chat for this to work and must have the 'can_pin_messages'
 * admin right in a supergroup or 'can_edit_messages' admin right in a channel.
 * Returns True on success.
 * @package tgbot\CoreAPI\Telegram\Methods
 * @see https://core.telegram.org/bots/api#unpinallchatmessages
 */
class UnpinAllChatMessages extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * @inheritDoc
     */
    public function requiredFields(): array
    {
        return ['chat_id'];
    }

    /**
     * @inheritDoc
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function beforeSending()
    {}
}