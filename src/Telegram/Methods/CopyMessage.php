<?php


namespace tgbot\CoreAPI\Telegram\Methods;


use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;
use tgbot\CoreAPI\Telegram\Types\MessageEntity;
use tgbot\CoreAPI\Telegram\Types\MessageId;

/**
 * Use this method to copy messages of any kind.
 * Service messages and invoice messages can't be copied.
 * The method is analogous to the method forwardMessage, but the copied message
 * doesn't have a link to the original message. Returns the MessageId
 * of the sent message on success.
 * @package tgbot\CoreAPI\Telegram\Methods
 * @see https://core.telegram.org/bots/api#copymessage
 */
class CopyMessage extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Unique identifier for the chat where the original message was sent
     * (or channel username in the format @channelusername)
     * @var int|string
     */
    public $from_chat_id = 0;

    /**
     * Message identifier in the chat specified in from_chat_id
     * @var int
     */
    public int $message_id = 0;

    /**
     * New caption for media, 0-1024 characters after entities parsing.
     * If not specified, the original caption is kept
     * @var string
     */
    public string $caption = '';

    /**
     * Mode for parsing entities in the new caption. See formatting options for more details.
     * @see https://core.telegram.org/bots/api#formatting-options
     * @var string
     */
    public string $parse_mode = '';

    /**
     * List of special entities that appear in the new caption, which can be specified instead of parse_mode
     * @var MessageEntity[]
     */
    public array $caption_entities = [];

    /**
     * Sends the message silently. Users will receive a notification with no sound.
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * If the message is a reply, ID of the original message
     * @var int
     */
    public int $reply_to_message_id = 0;

    /**
     * Pass True, if the message should be sent even if the specified replied-to message is not found
     * @var bool|string
     */
    public bool $allow_sending_without_reply = false;

    /**
     * Optional. Inline keyboard attached to the message.
     * `login_url` buttons are represented as ordinary `url` buttons.
     * @var Keyboards|string
     */
    public $reply_markup;

    /**
     * @inheritDoc
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'from_chat_id', 'message_id'];
    }

    /**
     * @inheritDoc
     */
    public function bindToObject($data)
    {
        return new MessageId($data);
    }

    /**
     * @inheritDoc
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && !is_string($this->reply_markup)) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}