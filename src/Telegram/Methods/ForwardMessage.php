<?php


namespace tgbot\CoreAPI\Telegram\Methods;


use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to forward messages of any kind. On success, the sent Message is returned.
 * @see https://core.telegram.org/bots/api#forwardmessage
 */
class ForwardMessage extends TelegramMethodsAbstract
{
    /**
     * @var int
     */
    public int $chat_id = 0;

    /**
     * @var int
     */
    public int $from_chat_id = 0;

    /**
     * @var bool
     */
    public bool $disable_notification = false;

    /**
     * @var int
     */
    public int $message_id = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array {
        return ['chat_id', 'from_chat_id', 'message_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}