<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\ChatPermissions;

/**
 * Use this method to restrict a user in a supergroup.
 * The bot must be an administrator in the supergroup for this to work and must
 * have the appropriate admin rights. Pass True for all permissions to lift
 * restrictions from a user. Returns True on success.
 * @package tgbot\CoreAPI\Methods
 */
class RestrictChatMember extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target supergroup
     * (in the format @supergroupusername)
     * @var int|string
     */
    public $chat_id = 0;

    /**
     * Unique identifier of the target user
     * @var int
     */
    public int $user_id = 0;

    /**
     * New user permissions
     * @var ChatPermissions
     */
    public ChatPermissions $permissions;

    /**
     * Date when restrictions will be lifted for the user, unix time.
     * If user is restricted for more than 366 days or less than 30 seconds
     * from the current time, they are considered to be restricted forever
     * @var int
     */
    public int $until_date = 0;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'user_id', 'permissions'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return $data;
    }

    /**
     * @return void
     */
    public function beforeSending()
    {}
}