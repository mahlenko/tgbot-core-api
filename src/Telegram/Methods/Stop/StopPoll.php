<?php

namespace tgbot\CoreAPI\Telegram\Methods\Stop;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to stop a poll which was sent by the bot. On success,
 * the stopped Poll with the final results is returned.
 * @package tgbot\CoreAPI\Methods
 */
class StopPoll extends TelegramMethodsAbstract
{
    /**
     * Unique identifier for the target chat or username of the target channel
     * (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Identifier of the original message with the poll
     * @var int
     */
    public int $message_id = 0;

    /**
     * A JSON-serialized object for a new message inline keyboard.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['chat_id', 'message_id'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return is_bool($data) ? $data : new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}