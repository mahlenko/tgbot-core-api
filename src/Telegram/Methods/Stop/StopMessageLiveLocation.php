<?php

namespace tgbot\CoreAPI\Telegram\Methods\Stop;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Message;

/**
 * Use this method to stop updating a live location message before live_period
 * expires. On success, if the message was sent by the bot, the sent Message is
 * returned, otherwise True is returned.
 * @package tgbot\CoreAPI\Methods
 * @see https://core.telegram.org/bots/api#stopmessagelivelocation
 */
class StopMessageLiveLocation extends TelegramMethodsAbstract
{
    /**
     * Required if inline_message_id is not specified. Unique identifier for the
     * target chat or username of the target channel (in the format @channelusername)
     * @var int
     */
    public int $chat_id = 0;

    /**
     * Required if inline_message_id is not specified. Identifier of the message
     * with live location to stop
     * @var int
     */
    public int $message_id = 0;

    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * A JSON-serialized object for a new inline keyboard.
     * @var InlineKeyboardMarkup|string
     */
    public $reply_markup;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new Message($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        if (!empty($this->reply_markup) && $this->reply_markup instanceof InlineKeyboardMarkup) {
            $this->reply_markup = json_encode(array_filter((array) $this->reply_markup));
        }
    }
}