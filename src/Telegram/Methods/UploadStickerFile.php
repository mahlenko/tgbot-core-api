<?php


namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;
use tgbot\CoreAPI\Telegram\Types\File;

/**
 * Use this method to upload a .png file with a sticker for later use in
 * createNewStickerSet and addStickerToSet methods (can be used multiple times).
 * Returns the uploaded File on success.
 * @see https://core.telegram.org/bots/api#uploadstickerfile
 */
class UploadStickerFile extends TelegramMethodsAbstract
{
    /**
     * User identifier of sticker file owner
     * @var int
     */
    public int $user_id = 0;

    /**
     * Png image with the sticker, must be up to 512 kilobytes in size,
     * dimensions must not exceed 512px, and either width or height must be
     * exactly 512px.
     * @var string
     */
    public string $png_sticker;

    /**
     * Request fields
     * @return array
     */
    public function requiredFields(): array
    {
        return ['user_id', 'png_sticker'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function bindToObject($data)
    {
        return new File($data);
    }

    /**
     * @return void
     */
    public function beforeSending()
    {
        $this->png_sticker = $this->attachFile($this->png_sticker);
    }
}