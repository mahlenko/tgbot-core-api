<?php

namespace tgbot\CoreAPI\Telegram\Methods;

use tgbot\CoreAPI\Abstracts\TelegramMethodsAbstract;

/**
 * Use this method to get the current default administrator rights of the bot.
 * Returns ChatAdministratorRights on success.
 * @see https://core.telegram.org/bots/api#getmydefaultadministratorrights
 */
class GetMyDefaultAdministratorRights extends TelegramMethodsAbstract
{
    /**
     * Pass True to get default administrator rights of the bot in channels.
     * Otherwise, default administrator rights of the bot for groups and supergroups will be returned.
     * @var bool
     */
    public bool $for_channels;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function requiredFields(): array
    {
        return [];
    }

    public function bindToObject($data)
    {
        return $data;
    }

    public function beforeSending()
    {
        //...
    }
}