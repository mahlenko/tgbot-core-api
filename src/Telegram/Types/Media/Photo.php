<?php

namespace tgbot\CoreAPI\Telegram\Types\Media;

use tgbot\CoreAPI\Telegram\Types\InputMedia;

/**
 * Represents a photo to be sent.
 * @see https://core.telegram.org/bots/api#inputmediaphoto
 */
class Photo extends InputMedia
{
    /**
     * Type of the result, must be photo
     * @var string
     */
    public string $type = 'photo';
}