<?php


namespace tgbot\CoreAPI\Telegram\Types\Media;

use tgbot\CoreAPI\Telegram\Types\InputFile;
use tgbot\CoreAPI\Telegram\Types\InputMedia;

/**
 * Represents a general file to be sent.
 * @see https://core.telegram.org/bots/api#inputmediadocument
 */
class Document extends InputMedia
{
    /**
     * Type of the result, must be document
     * @var string
     */
    public string $type = 'document';

    /**
     * Optional. Document thumbnail as defined by sender
     * @var InputFile
     */
    public InputFile $thumb;

    /**
     * Optional. Disables automatic server-side content type detection for files
     * uploaded using multipart/form-data. Always true, if the document is sent
     * as part of an album.
     * @var bool
     */
    public bool $disable_content_type_detection;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputFile::class => 'thumb'
        ];
    }
}