<?php

namespace tgbot\CoreAPI\Telegram\Types\Media;

use tgbot\CoreAPI\Telegram\Types\InputFile;
use tgbot\CoreAPI\Telegram\Types\InputMedia;

/**
 * Represents an animation file (GIF or H.264/MPEG-4 AVC video without sound) to be sent.
 * @see https://core.telegram.org/bots/api#inputmediaanimation
 */
class Animation extends InputMedia
{
    /**
     * Type of the result, must be animation
     * @var string
     */
    public string $type = 'animation';

    /**
     * Optional. Thumbnail of the file sent; can be ignored if thumbnail
     * generation for the file is supported server-side. The thumbnail should
     * be in JPEG format and less than 200 kB in size. A thumbnail‘s width and
     * height should not exceed 320. Ignored if the file is not uploaded using
     * multipart/form-data. Thumbnails can’t be reused and can be only uploaded
     * as a new file, so you can pass “attach://<file_attach_name>”
     * if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
     * @var InputFile
     */
    public InputFile $thumb;

    /**
     * Optional. Video width
     * @var int
     */
    public int $width = 0;

    /**
     * Optional. Video height
     * @var int
     */
    public int $height = 0;

    /**
     * Optional. Video duration
     * @var int
     */
    public int $duration = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputFile::class => 'thumb'
        ];
    }
}