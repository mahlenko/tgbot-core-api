<?php


namespace tgbot\CoreAPI\Telegram\Types\Media;

use tgbot\CoreAPI\Telegram\Types\InputFile;
use tgbot\CoreAPI\Telegram\Types\InputMedia;

/**
 * Represents an audio file to be treated as music to be sent.
 * @see https://core.telegram.org/bots/api#inputmediaaudio
 */
class Audio extends InputMedia
{
    /**
     * Type of the result, must be video
     * @var string
     */
    public string $type = 'audio';

    /**
     * Optional. Thumbnail of the file sent; can be ignored if thumbnail
     * generation for the file is supported server-side. The thumbnail should
     * be in JPEG format and less than 200 kB in size. A thumbnail‘s width and
     * height should not exceed 320. Ignored if the file is not uploaded using
     * multipart/form-data. Thumbnails can’t be reused and can be only uploaded
     * as a new file, so you can pass “attach://<file_attach_name>”
     * if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
     * @var InputFile
     */
    public InputFile $thumb;

    /**
     * Optional. Video duration
     * @var int
     */
    public int $duration = 0;

    /**
     * Optional. Performer of the audio
     * @var string
     */
    public string $performer = '';

    /**
     * Optional. Title of the audio
     * @var string
     */
    public string $title = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputFile::class => 'thumb'
        ];
    }

}