<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Represents a location to which a chat is connected.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#chatlocation
 */
class ChatLocation extends TelegramTypesAbstract
{
    /**
     * The location to which the supergroup is connected. Can't be a live location.
     * @var Location
     */
    public Location $location;

    /**
     * Location address; 1-64 characters, as defined by the chat owner
     * @var string
     */
    public string $address = '';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            Location::class => 'location'
        ];
    }
}