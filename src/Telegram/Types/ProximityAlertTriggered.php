<?php


namespace tgbot\CoreAPI\Telegram\Types;


use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents the content of a service message, sent whenever
 * a user in the chat triggers a proximity alert set by another user.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#proximityalerttriggered
 */
class ProximityAlertTriggered extends TelegramTypesAbstract
{
    /**
     * User that triggered the alert
     * @var User
     */
    public User $traveler;

    /**
     * User that set the alert
     * @var User
     */
    public User $watcher;

    /**
     * The distance between the users
     * @var int
     */
    public int $distance;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            User::class => ['traveler', 'watcher'],
        ];
    }
}