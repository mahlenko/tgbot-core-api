<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one result of an inline query.
 * Telegram clients currently support results of the following 20 types:
 *
 *   - InlineQueryResultCachedAudio
 *   - InlineQueryResultCachedDocument
 *   - InlineQueryResultCachedGif
 *   - InlineQueryResultCachedMpeg4Gif
 *   - InlineQueryResultCachedPhoto
 *   - InlineQueryResultCachedSticker
 *   - InlineQueryResultCachedVideo
 *   - InlineQueryResultCachedVoice
 *   - InlineQueryResultArticle
 *   - InlineQueryResultAudio
 *   - InlineQueryResultContact
 *   - InlineQueryResultGame
 *   - InlineQueryResultDocument
 *   - InlineQueryResultGif
 *   - InlineQueryResultLocation
 *   - InlineQueryResultMpeg4Gif
 *   - InlineQueryResultPhoto
 *   - InlineQueryResultVenue
 *   - InlineQueryResultVideo
 *   - InlineQueryResultVoice
 *
 * @see https://core.telegram.org/bots/api#inlinequeryresult
 */
class InlineQueryResult extends TelegramTypesAbstract
{
    /**
     * Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode
     * @var MessageEntity[]
     */
    public array $caption_entities = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}