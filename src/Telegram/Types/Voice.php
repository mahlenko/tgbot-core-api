<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a voice note.
 * @see https://core.telegram.org/bots/api#voice
 */
class Voice extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public string $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over
     * time and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public string $file_unique_id = '';

    /**
     * Duration of the audio in seconds as defined by sender
     * @var int
     */
    public int $duration = 0;

    /**
     * Optional. MIME type of the file as defined by sender
     * @var string
     */
    public string $mime_type = '';

    /**
     * Optional. File size
     * @var int
     */
    public int $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}