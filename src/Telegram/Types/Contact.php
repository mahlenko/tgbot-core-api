<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a phone contact.
 * @see https://core.telegram.org/bots/api#contact
 */
class Contact extends TelegramTypesAbstract
{
    /**
     * Contact's phone number
     * @var string
     */
    public string $phone_number = '';

    /**
     * Contact's first name
     * @var string
     */
    public string $first_name = '';

    /**
     * Optional. Contact's last name
     * @var string
     */
    public string $last_name = '';

    /**
     * Optional. Contact's user identifier in Telegram
     * @var int
     */
    public int $user_id = 0;

    /**
     * Optional. Additional data about the contact in the form of a vCard
     * @var string
     */
    public string $vcard = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}