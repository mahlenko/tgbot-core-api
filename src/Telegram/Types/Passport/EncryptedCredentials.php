<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Contains data required for decrypting and authenticating EncryptedPassportElement.
 * See the Telegram Passport Documentation for a complete description of the data
 * decryption and authentication processes.
 * @see EncryptedCredentials
 */
class EncryptedCredentials extends TelegramTypesAbstract
{
    /**
     * Base64-encoded encrypted JSON-serialized data with unique user's payload,
     * data hashes and secrets required for EncryptedPassportElement decryption
     * and authentication
     * @var string
     */
    public string $data = '';

    /**
     * Base64-encoded data hash for data authentication
     * @var string
     */
    public string $hash = '';

    /**
     * Base64-encoded secret, encrypted with the bot's public RSA key,
     * required for data decryption
     * @var string
     */
    public string $secret = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}