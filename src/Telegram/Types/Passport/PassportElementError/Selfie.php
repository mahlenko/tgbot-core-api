<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

use tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue with the selfie with a document.
 * The error is considered resolved when the file with the selfie changes.
 * @see https://core.telegram.org/bots/api#passportelementerrorselfie
 */
class Selfie extends PassportElementError
{
    /**
     * The section of the user's Telegram Passport which has the issue, one of
     * “passport”, “driver_license”, “identity_card”, “internal_passport”
     * @var string
     */
    public string $type = '';

    /**
     * Base64-encoded hash of the file with the selfie
     * @var string
     */
    public string $file_hash = '';
}