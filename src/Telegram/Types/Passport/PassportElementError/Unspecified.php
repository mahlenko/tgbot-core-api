<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

use tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue in an unspecified place. The error is considered
 * resolved when new data is added.
 * @see https://core.telegram.org/bots/api#passportelementerrorunspecified
 */
class Unspecified extends PassportElementError
{
    /**
     * Type of element of the user's Telegram Passport which has the issue
     * @var string
     */
    public string $type = '';

    /**
     * Base64-encoded element hash
     * @var string
     */
    public string $element_hash = '';
}