<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

use tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue with a list of scans. The error is considered resolved
 * when the list of files containing the scans changes.
 * @see https://core.telegram.org/bots/api#passportelementerrorfiles
 */
class Files extends PassportElementError
{
    /**
     * The section of the user's Telegram Passport which has the issue, one of
     * “utility_bill”, “bank_statement”, “rental_agreement”,
     * “passport_registration”, “temporary_registration”
     * @var string
     */
    public string $type = '';

    /**
     * List of base64-encoded file hashes
     * @var array
     */
    public array $file_hashes = [];
}