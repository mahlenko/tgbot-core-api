<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;


use tgbot\CoreAPI\Telegram\Types\Passport\PassportElementError;

/**
 * Represents an issue in one of the data fields that was provided by the user.
 * The error is considered resolved when the field's value changes.
 * @see https://core.telegram.org/bots/api#passportelementerrordatafield
 */
class DataField extends PassportElementError
{
    /**
     * Name of the data field which has the error
     * @var string
     */
    public string $field_name = '';

    /**
     * Base64-encoded data hash
     * @var string
     */
    public string $data_hash = '';
}