<?php

namespace tgbot\CoreAPI\Telegram\Types\Passport;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

class PassportElementError extends TelegramTypesAbstract
{
    /**
     * Error source, must be data
     * @var string
     */
    public string $source = '';

    /**
     * The section of the user's Telegram Passport which has the error, one of
     * “personal_details”, “passport”, “driver_license”, “identity_card”,
     * “internal_passport”, “address”
     * @var string
     */
    public string $type = '';

    /**
     * Error message
     * @var string
     */
    public string $message = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}