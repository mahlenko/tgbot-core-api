<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a bot command.
 * @package tgbot\CoreAPI\Types
 */
class BotCommand extends TelegramTypesAbstract
{
    /**
     * Text of the command, 1-32 characters. Can contain only lowercase English
     * letters, digits and underscores.
     * @var string
     */
    public string $command = '';

    /**
     * Description of the command, 3-256 characters.
     * @var string
     */
    public string $description = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}
