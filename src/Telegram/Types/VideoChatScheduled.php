<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a service message about a voice chat scheduled in the chat.
 * @package tgbot\CoreAPI\Telegram\Types
 */
class VideoChatScheduled extends TelegramTypesAbstract
{
    /**
     * Point in time (Unix timestamp) when the voice chat is supposed to be started by a chat administrator
     * @var int
     */
    public int $start_date;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}