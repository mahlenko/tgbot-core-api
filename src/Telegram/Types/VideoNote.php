<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;


/**
 * This object represents a video message (available in Telegram apps as of v.4.0).
 * @see https://core.telegram.org/bots/api#videonote
 */
class VideoNote extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public string $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over time
     * and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public string $file_unique_id = '';

    /**
     * Video width and height (diameter of the video message) as defined by sender
     * @var int
     */
    public int $length = 0;

    /**
     * Duration of the video in seconds as defined by sender
     * @var int
     */
    public int $duration = 0;

    /**
     * Optional. Video thumbnail
     * @var PhotoSize
     */
    public PhotoSize $thumb;

    /**
     * Optional. File size
     * @var int
     */
    public int $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class => 'thumb'
        ];
    }
}