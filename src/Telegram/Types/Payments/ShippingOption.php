<?php

namespace tgbot\CoreAPI\Telegram\Types\Payments;
use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one shipping option.
 * @see https://core.telegram.org/bots/api#shippingoption
 */
class ShippingOption extends TelegramTypesAbstract
{
    /**
     * Shipping option identifier
     * @var string
     */
    public string $id = '';

    /**
     * Option title
     * @var string
     */
    public string $title = '';

    /**
     * List of price portions
     * @var LabeledPrice[]
     */
    public array $prices = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            LabeledPrice::class => 'prices:array'
        ];
    }
}