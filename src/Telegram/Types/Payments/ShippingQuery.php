<?php

namespace tgbot\CoreAPI\Telegram\Types\Payments;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\User;

/**
 * This object contains information about an incoming shipping query.
 * @see https://core.telegram.org/bots/api#shippingquery
 */
class ShippingQuery extends TelegramTypesAbstract
{
    /**
     * Unique query identifier
     * @var string
     */
    public string $id = '';

    /**
     * User who sent the query
     * @var User
     */
    public User $from;

    /**
     * Bot specified invoice payload
     * @var string
     */
    public string $invoice_payload = '';

    /**
     * User specified shipping address
     * @var ShippingAddress
     */
    public ShippingAddress $shipping_address;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class            => 'from',
            ShippingAddress::class => 'shipping_address'
        ];
    }
}