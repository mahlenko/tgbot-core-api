<?php

namespace tgbot\CoreAPI\Telegram\Types\Payments;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object contains basic information about an invoice.
 * @see https://core.telegram.org/bots/api#invoice
 */
class Invoice extends TelegramTypesAbstract
{
    /**
     * Product name
     * @var string
     */
    public string $title = '';

    /**
     * Product description
     * @var string
     */
    public string $description = '';

    /**
     * Unique bot deep-linking parameter that can be used to generate this invoice
     * @var string
     */
    public string $start_parameter = '';

    /**
     * Three-letter ISO 4217 currency code
     * @see https://core.telegram.org/bots/payments#supported-currencies
     * @var string
     */
    public string $currency = '';

    /**
     * Total price in the smallest units of the currency (integer, not float/double).
     * For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter
     * in currencies.json, it shows the number of digits past the decimal point
     * for each currency (2 for the majority of currencies).
     * @see https://core.telegram.org/bots/payments/currencies.json
     * @var int
     */
    public int $total_amount = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}