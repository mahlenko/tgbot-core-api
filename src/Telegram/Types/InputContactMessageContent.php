<?php

namespace tgbot\CoreAPI\Telegram\Types;

/**
 * Represents the content of a contact message to be sent as the result of an
 * inline query.
 * @package tgbot\CoreAPI\Types
 * @see https://core.telegram.org/bots/api#inputcontactmessagecontent
 */
class InputContactMessageContent extends InputMessageContent
{
    /**
     * Contact's phone number
     * @var string
     */
    public string $phone_number = '';

    /**
     * Contact's first name
     * @var string
     */
    public string $first_name = '';

    /**
     * Optional. Contact's last name
     * @var string
     */
    public string $last_name = '';

    /**
     * Optional. Additional data about the contact in the form of a vCard, 0-2048 bytes
     * @var string
     */
    public string $vcard = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}