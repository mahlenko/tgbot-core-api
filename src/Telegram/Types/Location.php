<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a point on the map.
 * @see https://core.telegram.org/bots/api#location
 */
class Location extends TelegramTypesAbstract
{
    /**
     * Longitude as defined by sender
     * @var float
     */
    public float $longitude = 0.0;

    /**
     * Latitude as defined by sender
     * @var float
     */
    public float $latitude = 0.0;

    /**
     * Optional. The radius of uncertainty for the location, measured in meters; 0-1500
     * @var float
     */
    public float $horizontal_accuracy = 0.0;

    /**
     * Optional. Time relative to the message sending date, during which the location can be updated, in seconds. For active live locations only.
     * @var int
     */
    public int $live_period;

    /**
     * Optional. The direction in which user is moving, in degrees; 1-360. For active live locations only.
     * @var int
     */
    public int $heading;

    /**
     * Optional. Maximum distance for proximity alerts about approaching another chat member, in meters. For sent live locations only.
     * @var int
     */
    public int $proximity_alert_radius;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}