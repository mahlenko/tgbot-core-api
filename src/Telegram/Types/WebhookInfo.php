<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

class WebhookInfo extends TelegramTypesAbstract
{
    /**
     * Webhook URL, may be empty if webhook is not set up
     * @var string
     */
    public string $url = '';

    /**
     * True, if a custom certificate was provided for webhook certificate checks
     * @var bool
     */
    public bool $has_custom_certificate = false;

    /**
     * Number of updates awaiting delivery
     * @var int
     */
    public int $pending_update_count = 0;

    /**
     * Optional. Currently used webhook IP address
     * @var string
     */
    public string $ip_address;

    /**
     * Optional. Unix time for the most recent error that happened when trying
     * to deliver an update via webhook
     * @var int
     */
    public int $last_error_date = 0;

    /**
     * Optional. Error message in human-readable format for the most recent error
     * that happened when trying to deliver an update via webhook
     * @var string
     */
    public string $last_error_message = '';

    /**
     * Optional. Unix time of the most recent error that happened when trying
     * to synchronize available updates with Telegram datacenters
     * @var int
     */
    public int $last_synchronization_error_date;

    /**
     * Optional. Maximum allowed number of simultaneous HTTPS connections
     * to the webhook for update delivery
     * @var int
     */
    public int $max_connections = 0;

    /**
     * Optional. A list of update types the bot is subscribed to.
     * Defaults to all update types
     * @var array
     */
    public array $allowed_updates = [];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}