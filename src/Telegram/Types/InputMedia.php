<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents the content of a media message to be sent. It should be one of
 *  - InputMediaAnimation
 *  - InputMediaDocument
 *  - InputMediaAudio
 *  - InputMediaPhoto
 *  - InputMediaVideo
 * @see https://core.telegram.org/bots/api#inputmedia
 */
class InputMedia extends TelegramTypesAbstract
{
    /**
     * Type of the result
     * @var string
     */
    public string $type = '';

    /**
     * File to send. Pass a file_id to send a file that exists on the Telegram
     * servers (recommended), pass an HTTP URL for Telegram to get a file from
     * the Internet, or pass “attach://<file_attach_name>” to upload a new one
     * using multipart/form-data under <file_attach_name> name.
     * @see https://core.telegram.org/bots/api#sending-files
     * @var string
     */
    public string $media = '';

    /**
     * Optional. Caption of the photo to be sent, 0-1024 characters after
     * entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode
     * @var MessageEntity[]
     */
    public array $caption_entities = [];

    /**
     * Optional. Mode for parsing entities in the photo caption. See formatting options for more details.
     * @see https://core.telegram.org/bots/api#formatting-options
     * @var string
     */
    public string $parse_mode = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            MessageEntity::class => 'caption_entities:array',
        ];
    }
}