<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\Keyboards\ForceReply;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Keyboards\Keyboards;

use tgbot\CoreAPI\Telegram\Types\Keyboards\ReplyKeyboardMarkup;
use tgbot\CoreAPI\Telegram\Types\Keyboards\ReplyKeyboardRemove;
use tgbot\CoreAPI\Telegram\Types\Passport\PassportData;
use tgbot\CoreAPI\Telegram\Types\Payments\Invoice;
use tgbot\CoreAPI\Telegram\Types\Payments\SuccessfulPayment;
use tgbot\CoreAPI\Telegram\Types\Stickers\Sticker;
use tgbot\CoreAPI\Telegram\Types\WebApp\Data;

/**
 * This object represents a message.
 * @see https://core.telegram.org/bots/api#message
 */
class Message extends TelegramTypesAbstract
{
    /**
     * Unique message identifier inside this chat
     * @var int
     */
    public int $message_id = 0;

    /**
     * Optional. Sender, empty for messages sent to channels
     * @var User
     */
    public User $from;

    /**
     * Optional. Sender of the message, sent on behalf of a chat.
     * The channel itself for channel messages.
     * The supergroup itself for messages from anonymous group administrators.
     * The linked channel for messages automatically forwarded to the discussion group
     * @var Chat
     */
    public Chat $sender_chat;

    /**
     * Date the message was sent in Unix time
     * @var int
     */
    public int $date = 0;

    /**
     * Conversation the message belongs to
     * @var Chat
     */
    public Chat $chat;

    /**
     * Optional. For forwarded messages, sender of the original message
     * @var User
     */
    public User $forward_from;

    /**
     * Optional. For messages forwarded from channels, information about the original channel
     * @var Chat
     */
    public Chat $forward_from_chat;

    /**
     * Optional. For messages forwarded from channels, identifier of the
     * original message in the channel
     * @var int
     */
    public int $forward_from_message_id = 0;

    /**
     * Optional. For messages forwarded from channels, signature of the
     * post author if present
     * @var string
     */
    public string $forward_signature = '';

    /**
     * Optional. Sender's name for messages forwarded from users who disallow adding
     * a link to their account in forwarded messages
     * @var string
     */
    public string $forward_sender_name = '';

    /**
     * Optional. For forwarded messages, date the original message was sent in Unix time
     * @var int
     */
    public int $forward_date = 0;

    /**
     * Optional. For replies, the original message. Note that the Message object
     * in this field will not contain further reply_to_message fields even if
     * it itself is a reply.
     * @var Message
     */
    public Message $reply_to_message;

    /**
     * Optional. Bot through which the message was sent
     * @var User
     */
    public User $via_bot;

    /**
     * Optional. Date the message was last edited in Unix time
     * @var int
     */
    public int $edit_date = 0;

    /**
     * Optional. The unique identifier of a media message group this message belongs to
     * @var string
     */
    public string $media_group_id = '';

    /**
     * Optional. Signature of the post author for messages in channels,
     * or the custom title of an anonymous group administrator
     * @var string
     */
    public string $author_signature = '';

    /**
     * Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters
     * @var string
     */
    public string $text = '';

    /**
     * Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
     * @var MessageEntity[]
     */
    public array $entities = [];

    /**
     * Optional. For messages with a caption, special entities like usernames,
     * URLs, bot commands, etc. that appear in the caption
     * @var MessageEntity[]
     */
    public array $caption_entities = [];

    /**
     * Optional. Message is an audio file, information about the file
     * @var Audio
     */
    public Audio $audio;

    /**
     * Optional. Message is a general file, information about the file
     * @var Document
     */
    public Document $document;

    /**
     * Optional. Message is an animation, information about the animation.
     * For backward compatibility, when this field is set, the document
     * field will also be set
     * @var Animation
     */
    public Animation $animation;

    /**
     * Optional. Message is a game, information about the game.
     * @see https://core.telegram.org/bots/api#games
     * @var Game
     */
    public Game $game;

    /**
     * Optional. Message is a photo, available sizes of the photo
     * @var PhotoSize[]
     */
    public array $photo = [];

    /**
     * Optional. Message is a sticker, information about the sticker
     * @var Sticker
     */
    public Sticker $sticker;

    /**
     * Optional. Message is a video, information about the video
     * @var Video
     */
    public Video $video;

    /**
     * Optional. Message is a voice message, information about the file
     * @var Voice
     */
    public Voice $voice;

    /**
     * Optional. Message is a video note, information about the video message
     * @var VideoNote
     */
    public VideoNote $video_note;

    /**
     * Optional. Caption for the animation, audio, document, photo, video or voice, 0-1024 characters
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. Message is a shared contact, information about the contact
     * @var Contact
     */
    public Contact $contact;

    /**
     * Optional. Message is a dice with random value from 1 to 6
     * @var Dice
     */
    public Dice $dice;

    /**
     * Optional. Message is a shared location, information about the location
     * @var Location
     */
    public Location $location;

    /**
     * Optional. Message is a venue, information about the venue
     * @var Venue
     */
    public Venue $venue;

    /**
     * Optional. Message is a native poll, information about the poll
     * @var Poll
     */
    public Poll $poll;

    /**
     * Optional. New members that were added to the group or supergroup
     * and information about them (the bot itself may be one of these members)
     * @var User
     */
    public User $new_chat_member;

    /**
     * Optional. New members that were added to the group or supergroup
     * and information about them (the bot itself may be one of these members)
     * @var User
     */
    public User $new_chat_participant;

    /**
     * Optional. New members that were added to the group or supergroup
     * and information about them (the bot itself may be one of these members)
     * @var User[]
     */
    public array $new_chat_members = [];

    /**
     * Optional. A member was removed from the group, information about them
     * (this member may be the bot itself)
     * @var User
     */
    public User $left_chat_member;

    /**
     * Optional. A chat title was changed to this value
     * @var string
     */
    public string $new_chat_title = '';

    /**
     * Optional. A chat photo was change to this value
     * @var PhotoSize[]
     */
    public array $new_chat_photo = [];

    /**
     * Optional. Service message: the chat photo was deleted
     * @var bool
     */
    public bool $delete_chat_photo = false;

    /**
     * Optional. Service message: the group has been created
     * @var bool
     */
    public bool $group_chat_created = false;

    /**
     * Optional. Service message: the supergroup has been created.
     * This field can‘t be received in a message coming through updates,
     * because bot can’t be a member of a supergroup when it is created.
     * It can only be found in reply_to_message if someone replies to
     * a very first message in a directly created supergroup.
     * @var bool
     */
    public bool $supergroup_chat_created = false;

    /**
     * Optional. Service message: the channel has been created.
     * This field can‘t be received in a message coming through updates,
     * because bot can’t be a member of a channel when it is created.
     * It can only be found in reply_to_message if someone replies to
     * a very first message in a channel.
     * @var bool
     */
    public bool $channel_chat_created = false;

    /**
     * @var MessageAutoDeleteTimerChanged
     */
    public MessageAutoDeleteTimerChanged $message_auto_delete_timer_changed;

    /**
     * Optional. The group has been migrated to a supergroup with the specified
     * identifier. This number may be greater than 32 bits and some programming
     * languages may have difficulty/silent defects in interpreting it.
     * But it is smaller than 52 bits, so a signed 64 bit integer or double-precision
     * float type are safe for storing this identifier.
     * @var int
     */
    public int $migrate_to_chat_id = 0;

    /**
     * Optional. The supergroup has been migrated from a group with the
     * specified identifier. This number may be greater than 32 bits and some
     * programming languages may have difficulty/silent defects in interpreting it.
     * But it is smaller than 52 bits, so a signed 64 bit integer or double-precision
     * float type are safe for storing this identifier.
     * @var int
     */
    public int $migrate_from_chat_id = 0;

    /**
     * Optional. Specified message was pinned. Note that the Message object in
     * this field will not contain further reply_to_message fields even if it
     * is itself a reply.
     * @var Message
     */
    public Message $pinned_message;

    /**
     * Optional. Message is an invoice for a payment, information about the invoice.
     * @see https://core.telegram.org/bots/api#payments
     * @var Invoice
     */
    public Invoice $invoice;

    /**
     * Optional. Message is a service message about a successful payment,
     * information about the payment.
     * @var SuccessfulPayment
     */
    public SuccessfulPayment $successful_payment;

    /**
     * Optional. The domain name of the website on which the user has logged in.
     * @see https://core.telegram.org/widgets/login
     * @var string
     */
    public string $connected_website = '';

    /**
     * Optional. Telegram Passport data
     * @var PassportData
     */
    public PassportData $passport_data;

    /**
     * Optional. Service message. A user in the chat triggered another user's
     * proximity alert while sharing Live Location.
     * @var ProximityAlertTriggered
     */
    public ProximityAlertTriggered $proximity_alert_triggered;

    /**
     * Optional. Service message: video chat scheduled
     * @var VideoChatScheduled
     */
    public VideoChatScheduled $video_chat_scheduled;

    /**
     * Optional. Service message: video chat started
     * @var VideoChatStarted
     */
    public VideoChatStarted $video_chat_started;

    /**
     * Optional. Service message: video chat ended
     * @var VideoChatEnded
     */
    public VideoChatEnded $video_chat_ended;

    /**
     * Optional. Service message: new participants invited to a video chat
     * @var VideoChatParticipantsInvited
     */
    public VideoChatParticipantsInvited $video_chat_participants_invited;

    /**
     * Optional. Inline keyboard attached to the message.
     * `login_url` buttons are represented as ordinary `url` buttons.
     * @var InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardRemove|ForceReply
     */
    public Keyboards $reply_markup;

    /**
     * Optional. Service message: data sent by a Web App
     * @var Data
     */
    public Data $web_app_data;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            Poll::class                         => 'poll',
            Chat::class                         => ['chat', 'sender_chat', 'forward_from_chat'],
            User::class                         => ['from','forward_from','left_chat_member','new_chat_member','new_chat_participant','new_chat_members:array','via_bot'],
            Game::class                         => 'game',
            Dice::class                         => 'dice',
            Audio::class                        => 'audio',
            Video::class                        => 'video',
            Voice::class                        => 'voice',
            Venue::class                        => 'venue',
            Contact::class                      => 'contact',
            Sticker::class                      => 'sticker',
            Invoice::class                      => 'invoice',
            Document::class                     => 'document',
            Location::class                     => 'location',
            PhotoSize::class                    => ['photo:array','new_chat_photo:array'],
            Animation::class                    => 'animation',
            VideoNote::class                    => 'video_note',
            PassportData::class                 => 'passport_data',
            Message::class                      => ['reply_to_message','pinned_message'],
            MessageEntity::class                => ['entities:array','caption_entities:array'],
            SuccessfulPayment::class            => 'successful_payment',
            InlineKeyboardMarkup::class         => 'reply_markup',
            ProximityAlertTriggered::class      => 'proximity_alert_triggered',
            VideoChatScheduled::class           => 'video_chat_scheduled',
            VideoChatStarted::class             => 'video_chat_started',
            VideoChatEnded::class               => 'video_chat_ended',
            VideoChatParticipantsInvited::class => 'video_chat_participants_invited',
            Data::class => 'web_app_data',
        ];
    }
}