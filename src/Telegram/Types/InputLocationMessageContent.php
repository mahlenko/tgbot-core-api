<?php

namespace tgbot\CoreAPI\Telegram\Types;

/**
 * Represents the content of a location message to be sent as the result of an
 * inline query.
 * @package tgbot\CoreAPI\Types
 * @see https://core.telegram.org/bots/api#inputlocationmessagecontent
 */
class InputLocationMessageContent extends InputMessageContent
{
    /**
     * Latitude of the location in degrees
     * @var float
     */
    public float $latitude = 0.0;

    /**
     * Longitude of the location in degrees
     * @var float
     */
    public float $longitude = 0.0;

    /**
     * Optional. The radius of uncertainty for the location, measured in meters; 0-1500
     * @var float
     */
    public float $horizontal_accuracy = 0.0;

    /**
     * Optional. Period in seconds for which the location can be updated,
     * should be between 60 and 86400.
     * @var int
     */
    public int $live_period = 0;

    /**
     * Optional. For live locations, a direction in which the user is moving, in degrees.
     * Must be between 1 and 360 if specified.
     * @var int
     */
    public int $heading;

    /**
     * Optional. For live locations, a maximum distance for proximity alerts about
     * approaching another chat member, in meters. Must be between 1 and 100000 if specified.
     * @var int
     */
    public int $proximity_alert_radius;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}