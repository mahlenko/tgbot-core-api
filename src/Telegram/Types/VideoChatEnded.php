<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a service message about a voice chat ended in the chat.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#voicechatended
 */
class VideoChatEnded extends TelegramTypesAbstract
{
    /**
     * Voice chat duration; in seconds
     * @var int
     */
    public int $duration = 0;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}