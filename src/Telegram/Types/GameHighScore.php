<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one row of the high scores table for a game.
 * @see https://core.telegram.org/bots/api#gamehighscore
 */
class GameHighScore extends TelegramTypesAbstract
{
    /**
     * Position in high score table for the game
     * @var int
     */
    public int $position = 0;

    /**
     * User
     * @var User
     */
    public User $user;

    /**
     * Score
     * @var int
     */
    public int $score = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class => 'user'
        ];
    }
}