<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a dice with a random value from 1 to 6 for currently
 * supported base emoji. (Yes, we're aware of the “proper” singular of die.
 * But it's awkward, and we decided to help it change. One dice at a time!)
 * @package tgbot\CoreAPI\Types
 */
class Dice extends TelegramTypesAbstract
{
    /**
     * Emoji on which the dice throw animation is based
     * @var string
     */
    public string $emoji = '';

    /**
     * Value of the dice, 1-6 for currently supported base emoji
     * @var int
     */
    public int $value = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}