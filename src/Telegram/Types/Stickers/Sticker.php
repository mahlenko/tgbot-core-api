<?php

namespace tgbot\CoreAPI\Telegram\Types\Stickers;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\InputFile;
use tgbot\CoreAPI\Telegram\Types\PhotoSize;


/**
 * This object represents a sticker.
 * @see https://core.telegram.org/bots/api#stickers
 */
class Sticker extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public string $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over time
     * and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public string $file_unique_id = '';

    /**
     * Sticker width
     * @var int
     */
    public int $width = 0;

    /**
     * Sticker height
     * @var int
     */
    public int $height = 0;

    /**
     * True, if the sticker is animated
     * @var bool
     */
    public bool $is_animated = false;

    /**
     * Optional. Sticker thumbnail in the .webp or .jpg format
     * @var PhotoSize
     */
    public PhotoSize $thumb;

    /**
     * Optional. Emoji associated with the sticker
     * @var string
     */
    public string $emoji = '';

    /**
     * Optional. Name of the sticker set to which the sticker belongs
     * @var string
     */
    public string $set_name = '';

    /**
     * Optional. For mask stickers, the position where the mask should be placed
     * @var MaskPosition
     */
    public MaskPosition $mask_position;

    /**
     * Optional. File size
     * @var int
     */
    public int $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class    => 'thumb',
            MaskPosition::class => 'mask_position'
        ];
    }
}