<?php

namespace tgbot\CoreAPI\Telegram\Types\Stickers;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\PhotoSize;


/**
 * This object represents a sticker set.
 * @see https://core.telegram.org/bots/api#stickerset
 */
class StickerSet extends TelegramTypesAbstract
{
    /**
     * Sticker set name
     * @var string
     */
    public string $name = '';

    /**
     * Sticker set title
     * @var string
     */
    public string $title = '';

    /**
     * True, if the sticker set contains animated stickers
     * @var bool
     */
    public bool $is_animated = false;

    /**
     * True, if the sticker set contains masks
     * @var bool
     */
    public bool $contains_masks = false;

    /**
     * List of all set stickers
     * @var Sticker[]
     */
    public array $stickers = [];

    /**
     * Optional. Sticker set thumbnail in the .WEBP or .TGS format
     * @var PhotoSize
     */
    public PhotoSize $thumb;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            Sticker::class   => 'stickers:array',
            PhotoSize::class => 'thumb'
        ];
    }
}