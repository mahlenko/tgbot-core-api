<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a service message about a change in auto-delete timer settings.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#messageautodeletetimerchanged
 */
class MessageAutoDeleteTimerChanged extends TelegramTypesAbstract
{
    /**
     * New auto-delete time for messages in the chat
     * @var int
     */
    public int $message_auto_delete_time = 0;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}