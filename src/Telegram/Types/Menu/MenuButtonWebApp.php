<?php

namespace tgbot\CoreAPI\Telegram\Types\Menu;

use tgbot\CoreAPI\Telegram\Types\WebApp\Info;

/**
 * Represents a menu button, which launches a Web App.
 * @see https://core.telegram.org/bots/api#menubuttonwebapp
 */
class MenuButtonWebApp extends MenuButton
{
    /**
     * Type of the button, must be web_app
     * @var string
     */
    public string $type = 'web_app';

    /**
     * Text on the button
     * @var string
     */
    public string $text = '';

    /**
     * Description of the Web App that will be launched when the user
     * presses the button. The Web App will be able to send an arbitrary
     * message on behalf of the user using the method answerWebAppQuery.
     * @var Info
     */
    public Info $web_app;
}