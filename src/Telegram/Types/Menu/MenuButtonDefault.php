<?php

namespace tgbot\CoreAPI\Telegram\Types\Menu;

/**
 * Describes that no specific value for the menu button was set.
 * @see https://core.telegram.org/bots/api#menubuttondefault
 */
class MenuButtonDefault extends MenuButton
{
    /**
     * Type of the button, must be default
     * @var string
     */
    public string $type = 'default';
}