<?php

namespace tgbot\CoreAPI\Telegram\Types;

/**
 * Represents a result of an inline query that was chosen by the user and sent
 * to their chat partner.
 *
 * | Note: It is necessary to enable inline feedback via @Botfather in order
 * | to receive these objects in updates.
 *
 * @package tgbot\CoreAPI\Types
 * @see https://core.telegram.org/bots/api#choseninlineresult
 */
class ChosenInlineResult extends InlineQueryResult
{
    /**
     * The unique identifier for the result that was chosen
     * @var string
     */
    public string $result_id = '';

    /**
     * The user that chose the result
     * @var User
     */
    public User $from;

    /**
     * Optional. Sender location, only for bots that require user location
     * @var Location
     */
    public Location $location;

    /**
     * Optional. Identifier of the sent inline message. Available only if there
     * is an inline keyboard attached to the message. Will be also received in
     * callback queries and can be used to edit the message.
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * The query that was used to obtain the result
     * @var string
     */
    public string $query = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class     => 'from',
            Location::class => 'location'
        ];
    }
}