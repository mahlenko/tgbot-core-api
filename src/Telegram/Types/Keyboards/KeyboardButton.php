<?php

namespace tgbot\CoreAPI\Telegram\Types\Keyboards;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\WebApp\Info;

/**
 * This object represents one button of the reply keyboard. For simple text
 * buttons String can be used instead of this object to specify text of the button.
 * Optional fields request_contact, request_location, and request_poll are
 * mutually exclusive.
 *
 * | Note: request_contact and request_location options will only work in Telegram
 * | versions released after 9 April, 2016. Older clients will display unsupported
 * | message.
 *
 * | Note: request_poll option will only work in Telegram versions released
 * | after 23 January, 2020. Older clients will display unsupported message.
 *
 * @see https://core.telegram.org/bots/api#keyboardbutton
 */
class KeyboardButton extends TelegramTypesAbstract
{
    /**
     * Text of the button. If none of the optional fields are used, it will be
     * sent as a message when the button is pressed
     * @var string
     */
    public string $text = '';

    /**
     * Optional. If True, the user's phone number will be sent as a contact when
     * the button is pressed. Available in private chats only
     * @var bool
     */
    public bool $request_contact = false;

    /**
     * Optional. If True, the user's current location will be sent when
     * the button is pressed. Available in private chats only
     * @var bool
     */
    public bool $request_location = false;

    /**
     * Optional. If specified, the user will be asked to create a poll and
     * send it to the bot when the button is pressed. Available in private
     * chats only
     * @var KeyboardButtonPollType
     */
    public KeyboardButtonPollType $request_poll;

    /**
     * Optional. If specified, the described Web App will be launched when
     * the button is pressed. The Web App will be able to send a “web_app_data”
     * service message. Available in private chats only.
     * @var Info
     */
    public Info $web_app;

    protected function fillObjectData($data = null) {
        if (! $data) {
            parent::fillObjectData($data);
            return;
        }

        $data = (array) $data;
        foreach ($data as $key => $value) {
            $this->$key = match($key) {
                'web_app' => new Info($value),
                default => $value
            };
        }
    }


    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            KeyboardButtonPollType::class => 'request_poll'
        ];
    }
}