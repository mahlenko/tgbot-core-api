<?php

namespace tgbot\CoreAPI\Telegram\Types\Keyboards;

class InlineKeyboardMarkup extends Keyboards
{
    /**
     * Array of button rows, each represented by an Array of InlineKeyboardButton objects
     * @var InlineKeyboardButton[]
     */
    public array $inline_keyboard = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }

    public function fillObjectData($data = null)
    {
        if (!$data) {
            parent::fillObjectData($data);
            return;
        }

        $data = (array) $data;

        foreach ($data['inline_keyboard'] as $index => $row) {
            $this->inline_keyboard[$index] = [];

            foreach ($row as $button) {
                $this->inline_keyboard[$index][] = new InlineKeyboardButton($button);
            }
        }
    }
}