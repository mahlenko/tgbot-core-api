<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a service message about a voice chat started in the chat. Currently holds no information.
 * @package tgbot\CoreAPI\Telegram\Types
 */
class VideoChatStarted extends TelegramTypesAbstract
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}