<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Represents a chat member that has some additional privileges.
 * @see https://core.telegram.org/bots/api#chatmemberadministrator
 */
class ChatMemberAdministrator extends TelegramTypesAbstract
{
    /**
     * The member's status in the chat, always “administrator”
     * @var string
     */
    public string $status = '';

    /**
     * Information about the user
     * @var User
     */
    public User $user;

    /**
     * True, if the bot is allowed to edit administrator privileges of that user
     * @var bool
     */
    public bool $can_be_edited = false;

    /**
     * True, if the user's presence in the chat is hidden
     * @var bool
     */
    public bool $is_anonymous = true;

    /**
     * Optional. Administrators only. True, if the administrator can access the
     * chat event log, chat statistics, message statistics in channels,
     * see channel members, see anonymous administrators in supergroups and
     * ignore slow mode. Implied by any other administrator privilege
     * @var bool
     */
    public bool $can_manage_chat = true;

    /**
     * Optional. Administrators only. True, if the administrator can delete
     * messages of other users
     * @var bool
     */
    public bool $can_delete_messages = false;

    /**
     * True, if the administrator can manage video chats
     * @var bool
     */
    public bool $can_manage_video_chats = true;

    /**
     * Optional. Administrators only. True, if the administrator can restrict,
     * ban or unban chat members
     * @var bool
     */
    public bool $can_restrict_members = false;

    /**
     * Optional. Administrators only. True, if the administrator can add new
     * administrators with a subset of his own privileges or demote administrators
     * that he has promoted, directly or indirectly (promoted by administrators
     * that were appointed by the user)
     * @var bool
     */
    public bool $can_promote_members = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to change the chat title, photo and other settings
     * @var bool
     */
    public bool $can_change_info = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to invite new users to the chat
     * @var bool
     */
    public bool $can_invite_users = false;

    /**
     * Optional. Administrators only. True, if the administrator can post in the
     * channel; channels only
     * @var bool
     */
    public bool $can_post_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can edit messages
     * of other users and can pin messages; channels only
     * @var bool
     */
    public bool $can_edit_messages = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to pin messages; groups and supergroups only
     * @var bool
     */
    public bool $can_pin_messages = false;

    /**
     * Optional. Owner and administrators only. Custom title for this user
     * @var string
     */
    public string $custom_title = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class => 'user'
        ];
    }
}