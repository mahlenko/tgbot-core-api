<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;
use tgbot\CoreAPI\Telegram\Types\Payments\PreCheckoutQuery;
use tgbot\CoreAPI\Telegram\Types\Payments\ShippingQuery;

/**
 * This object represents an incoming update.
 * @see https://core.telegram.org/bots/api#getting-updates
 */
class Update extends TelegramTypesAbstract
{
    /**
     * The update‘s unique identifier. Update identifiers start from a certain
     * positive number and increase sequentially. This ID becomes especially
     * handy if you’re using Webhooks, since it allows you to ignore repeated
     * updates or to restore the correct update sequence, should they get out
     * of order. If there are no new updates for at least a week, then identifier
     * of the next update will be chosen randomly instead of sequentially.
     * @var int
     */
    public int $update_id = 0;

    /**
     * Optional. New incoming message of any kind — text, photo, sticker, etc.
     * @var Message
     */
    public Message $message;

    /**
     * Optional. New version of a message that is known to the bot and was edited
     * @var Message
     */
    public Message $edited_message;

    /**
     * Optional. New incoming channel post of any kind — text, photo, sticker, etc.
     * @var Message
     */
    public Message $channel_post;

    /**
     * Optional. New version of a channel post that is known to the bot and was edited
     * @var Message
     */
    public Message $edited_channel_post;

    /**
     * Optional. New incoming inline query
     * @var InlineQuery
     */
    public InlineQuery $inline_query;

    /**
     * Optional. The result of an inline query that was chosen by a user and sent
     * to their chat partner. Please see our documentation on the feedback collecting
     * for details on how to enable these updates for your bot.
     * @var ChosenInlineResult
     */
    public ChosenInlineResult $chosen_inline_result;

    /**
     * Optional. New incoming callback query
     * @var CallbackQuery
     */
    public CallbackQuery $callback_query;

    /**
     * Optional. New incoming shipping query. Only for invoices with flexible price
     * @var ShippingQuery
     */
    public ShippingQuery $shipping_query;

    /**
     * Optional. New incoming pre-checkout query. Contains full information about checkout
     * @var PreCheckoutQuery
     */
    public PreCheckoutQuery $pre_checkout_query;

    /**
     * Optional. New poll state. Bots receive only updates about stopped polls
     * and polls, which are sent by the bot
     * @var Poll
     */
    public Poll $poll;

    /**
     * Optional. A user changed their answer in a non-anonymous poll.
     * Bots receive new votes only in polls that were sent by the bot itself.
     * @var PollAnswer
     */
    public PollAnswer $poll_answer;

    /**
     * Optional. The bot's chat member status was updated in a chat.
     * For private chats, this update is received only when the bot is blocked
     * or unblocked by the user.
     * @var ChatMemberUpdated
     */
    public ChatMemberUpdated $my_chat_member;

    /**
     * Optional. A chat member's status was updated in a chat.
     * The bot must be an administrator in the chat and must explicitly specify
     * "chat_member" in the list of allowed_updates to receive these updates.
     * @var ChatMemberUpdated
     */
    public ChatMemberUpdated $chat_member;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            Poll::class               => 'poll',
            Message::class            => ['message', 'edited_message','channel_post','edited_channel_post'],
            PollAnswer::class         => 'pool_answer',
            InlineQuery::class        => 'inline_query',
            CallbackQuery::class      => 'callback_query',
            ShippingQuery::class      => 'shipping_query',
            PreCheckoutQuery::class   => 'pre_checkout_query',
            ChosenInlineResult::class => 'chosen_inline_result',
            ChatMemberUpdated::class  => ['my_chat_member', 'chat_member'],
        ];
    }
}