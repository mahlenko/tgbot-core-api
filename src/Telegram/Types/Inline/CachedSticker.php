<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a sticker stored on the Telegram servers. By default,
 * this sticker will be sent by the user. Alternatively, you can use
 * input_message_content to send a message with the specified content instead
 * of the sticker.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016
 * | for static stickers and after 06 July, 2019 for animated stickers. Older
 * | clients will ignore them.
 *
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultcachedsticker
 */
class CachedSticker extends InlineQueryResult
{
    /**
     * Type of the result, must be sticker
     * @var string
     */
    public string $type = 'sticker';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public string $id = '';

    /**
     * A valid file identifier of the sticker
     * @var string
     */
    public string $sticker_file_id = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the sticker
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}