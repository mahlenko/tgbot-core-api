<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to an MP3 audio file. By default, this audio file will
 * be sent by the user. Alternatively, you can use input_message_content to
 * send a message with the specified content instead of the audio.
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultaudio
 */
class Audio extends InlineQueryResult
{
    /**
     * Type of the result, must be audio
     * @var string
     */
    public string $type = 'audio';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public string $id = '';

    /**
     * A valid URL for the audio file
     * @var string
     */
    public string $audio_url = '';

    /**
     * Title
     * @var string
     */
    public string $title = '';

    /**
     * Optional. Caption, 0-1024 characters after entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public string $parse_mode = '';

    /**
     * Optional. Performer
     * @var string
     */
    public string $performer = '';

    /**
     * Optional. Audio duration in seconds
     * @var string
     */
    public string $audio_duration = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the audio
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}