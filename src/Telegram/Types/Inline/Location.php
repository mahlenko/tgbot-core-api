<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a location on a map. By default, the location will be sent by the
 * user. Alternatively, you can use input_message_content to send a message with
 * the specified content instead of the location.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultlocation
 */
class Location extends InlineQueryResult
{
    /**
     * Type of the result, must be location
     * @var string
     */
    public string $type = 'location';

    /**
     * Unique identifier for this result, 1-64 Bytes
     * @var string
     */
    public string $id = '';

    /**
     * Location latitude in degrees
     * @var float
     */
    public float $latitude = 0.0;

    /**
     * Location longitude in degrees
     * @var float
     */
    public float $longitude = 0.0;

    /**
     * Location title
     * @var string
     */
    public string $title = '';

    /**
     * Optional. The radius of uncertainty for the location, measured in meters; 0-1500
     * @var float
     */
    public float $horizontal_accuracy = 0.0;

    /**
     * Optional. Period in seconds for which the location can be updated,
     * should be between 60 and 86400.
     * @var int
     */
    public int $live_period = 0;

    /**
     * Optional. For live locations, a direction in which the user is moving, in degrees.
     * Must be between 1 and 360 if specified.
     * @var int
     */
    public int $heading;

    /**
     * Optional. For live locations, a maximum distance for proximity alerts about
     * approaching another chat member, in meters. Must be between 1 and 100000 if specified.
     * @var int
     */
    public int $proximity_alert_radius;

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the location
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * Optional. Url of the thumbnail for the result
     * @var string
     */
    public string $thumb_url = '';

    /**
     * Optional. Thumbnail width
     * @var int
     */
    public int $thumb_width = 0;

    /**
     * Optional. Thumbnail height
     * @var int
     */
    public int $thumb_height = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}