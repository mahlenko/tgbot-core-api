<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a voice message stored on the Telegram servers.
 * By default, this voice message will be sent by the user. Alternatively,
 * you can use input_message_content to send a message with the specified
 * content instead of the voice message.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultcachedvoice
 */
class CachedVoice extends InlineQueryResult
{
    /**
     * Type of the result, must be voice
     * @var string
     */
    public string $type = 'voice';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public string $id = '';

    /**
     * A valid file identifier for the voice message
     * @var string
     */
    public string $voice_file_id = '';

    /**
     * Voice message title
     * @var string
     */
    public string $title = '';

    /**
     * Optional. Caption, 0-1024 characters after entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public string $parse_mode = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the voice message
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}