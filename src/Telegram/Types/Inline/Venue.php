<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a venue. By default, the venue will be sent by the user.
 * Alternatively, you can use input_message_content to send a message with
 * the specified content instead of the venue.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultvenue
 */
class Venue extends InlineQueryResult
{
    /**
     * Type of the result, must be venue
     * @var string
     */
    public string $type = 'venue';

    /**
     * Unique identifier for this result, 1-64 Bytes
     * @var string
     */
    public string $id = '';

    /**
     * Latitude of the venue location in degrees
     * @var float
     */
    public float $latitude = 0.0;

    /**
     * Longitude of the venue location in degrees
     * @var float
     */
    public float $longitude = 0.0;

    /**
     * Title of the venue
     * @var string
     */
    public string $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public string $address = '';

    /**
     * Optional. Foursquare identifier of the venue if known
     * @var string
     */
    public string $foursquare_id = '';

    /**
     * Optional. Foursquare type of the venue, if known. (For example,
     * “arts_entertainment/default”, “arts_entertainment/aquarium” or
     * “food/icecream”.)
     * @var string
     */
    public string $foursquare_type = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the venue
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * Optional. Url of the thumbnail for the result
     * @var string
     */
    public string $thumb_url = '';

    /**
     * Optional. Thumbnail width
     * @var int
     */
    public int $thumb_width = 0;

    /**
     * Optional. Thumbnail height
     * @var int
     */
    public int $thumb_height = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}