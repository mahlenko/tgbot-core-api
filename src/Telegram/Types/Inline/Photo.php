<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a photo. By default, this photo will be sent by the user
 * with optional caption. Alternatively, you can use input_message_content to send
 * a message with the specified content instead of the photo.
 * @see https://core.telegram.org/bots/api#inlinequeryresultphoto
 */
class Photo extends InlineQueryResult
{
    /**
     * Type of the result, must be photo
     * @var string
     */
    public string $type = 'photo';

    /**
     * Unique identifier for this result, 1-64 Bytes
     * @var string
     */
    public string $id = '';

    /**
     * A valid URL of the photo. Photo must be in jpeg format.
     * Photo size must not exceed 5MB
     * @var string
     */
    public string $photo_url = '';

    /**
     * URL of the thumbnail for the photo
     * @var string
     */
    public string $thumb_url = '';

    /**
     * Optional. Width of the photo
     * @var int
     */
    public int $photo_width = 0;

    /**
     * Optional. Height of the photo
     * @var int
     */
    public int $photo_height = 0;

    /**
     * Optional. Title for the result
     * @var string
     */
    public string $title = '';

    /**
     * Optional. Short description of the result
     * @var string
     */
    public string $description = '';

    /**
     * Optional. Caption of the photo to be sent, 0-1024 characters after
     * entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public string $parse_mode = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the photo
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}