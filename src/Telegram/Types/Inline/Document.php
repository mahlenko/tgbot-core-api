<?php

namespace tgbot\CoreAPI\Telegram\Types\Inline;

use tgbot\CoreAPI\Telegram\Types\InlineQueryResult;
use tgbot\CoreAPI\Telegram\Types\InputMessageContent;
use tgbot\CoreAPI\Telegram\Types\Keyboards\InlineKeyboardMarkup;

/**
 * Represents a link to a file. By default, this file will be sent by the user
 * with an optional caption. Alternatively, you can use input_message_content
 * to send a message with the specified content instead of the file. Currently,
 * only .PDF and .ZIP files can be sent using this method.
 *
 * | Note: This will only work in Telegram versions released after 9 April, 2016.
 * | Older clients will ignore them.
 *
 * @package tgbot\CoreAPI\Types\Inline
 * @see https://core.telegram.org/bots/api#inlinequeryresultdocument
 */
class Document extends InlineQueryResult
{
    /**
     * Type of the result, must be document
     * @var string
     */
    public string $type = 'document';

    /**
     * Unique identifier for this result, 1-64 bytes
     * @var string
     */
    public string $id = '';

    /**
     * Title for the result
     * @var string
     */
    public string $title = '';

    /**
     * Optional. Caption of the document to be sent, 0-1024 characters after
     * entities parsing
     * @var string
     */
    public string $caption = '';

    /**
     * Optional. Send Markdown or HTML, if you want Telegram apps to show bold,
     * italic, fixed-width text or inline URLs in the media caption.
     * @var string
     */
    public string $parse_mode = '';

    /**
     * A valid URL for the file
     * @var string
     */
    public string $document_url = '';

    /**
     * Mime type of the content of the file, either “application/pdf” or “application/zip”
     * @var string
     */
    public string $mime_type = '';

    /**
     * Optional. Short description of the result
     * @var string
     */
    public string $description = '';

    /**
     * Optional. Inline keyboard attached to the message
     * @var InlineKeyboardMarkup
     */
    public InlineKeyboardMarkup $reply_markup;

    /**
     * Optional. Content of the message to be sent instead of the file
     * @var InputMessageContent
     */
    public InputMessageContent $input_message_content;

    /**
     * Optional. URL of the thumbnail (jpeg only) for the file
     * @var string
     */
    public string $thumb_url = '';

    /**
     * Optional. Thumbnail width
     * @var int
     */
    public int $thumb_width = 0;

    /**
     * Optional. Thumbnail height
     * @var int
     */
    public int $thumb_height = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            InputMessageContent::class  => 'input_message_content',
            InlineKeyboardMarkup::class => 'reply_markup'
        ];
    }
}