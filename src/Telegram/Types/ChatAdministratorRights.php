<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Represents the rights of an administrator in a chat.
 * @see https://core.telegram.org/bots/api#chatadministratorrights
 */
class ChatAdministratorRights extends TelegramTypesAbstract
{
    /**
     * True, if the user's presence in the chat is hidden
     * @var bool
     */
    public bool $is_anonymous = true;

    /**
     * True, if the administrator can access the chat event log, chat statistics,
     * message statistics in channels, see channel members, see anonymous
     * administrators in supergroups and ignore slow mode.
     * Implied by any other administrator privilege
     * @var bool
     */
    public bool $can_manage_chat = true;

    /**
     * True, if the administrator can delete messages of other users.
     * @var bool
     */
    public bool $can_delete_messages = true;

    /**
     * True, if the administrator can manage video chats
     * @var bool
     */
    public bool $can_manage_video_chats = true;

    /**
     * True, if the administrator can restrict, ban or unban chat members
     * @var bool
     */
    public bool $can_restrict_members = true;

    /**
     * True, if the administrator can add new administrators with a subset of their
     * own privileges or demote administrators that he has promoted,
     * directly or indirectly (promoted by administrators that were appointed by the user)
     * @var bool
     */
    public bool $can_promote_members = true;

    /**
     * True, if the user is allowed to change the chat title, photo and other settings
     * @var bool
     */
    public bool $can_change_info = true;

    /**
     * True, if the user is allowed to invite new users to the chat
     * @var bool
     */
    public bool $can_invite_users = true;

    /**
     * Optional. True, if the administrator can post in the channel; channels only
     * @var bool
     */
    public bool $can_post_messages = true;

    /**
     * Optional. True, if the administrator can edit messages of other users and can pin messages; channels only
     * @var bool
     */
    public bool $can_edit_messages = true;

    /**
     * Optional. True, if the user is allowed to pin messages; groups and supergroups only
     * @var bool
     */
    public bool $can_pin_messages = true;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}