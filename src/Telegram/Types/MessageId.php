<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a unique message identifier.
 * @package tgbot\CoreAPI\Telegram\Types
 */
class MessageId extends TelegramTypesAbstract
{
    /**
     * Unique message identifier
     * @var int
     */
    public int $message_id = 0;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [];
    }
}