<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents one size of a photo or a file / sticker thumbnail.
 * @see https://core.telegram.org/bots/api#photosize
 */
class PhotoSize extends TelegramTypesAbstract
{
    /**
     * Identifier for this file, which can be used to download or reuse the file
     * @var string
     */
    public string $file_id = '';

    /**
     * Unique identifier for this file, which is supposed to be the same over
     * time and for different bots. Can't be used to download or reuse the file.
     * @var string
     */
    public string $file_unique_id = '';

    /**
     * Photo width
     * @var int
     */
    public int $width = 0;

    /**
     * Photo height
     * @var int
     */
    public int $height = 0;

    /**
     * Optional. File size
     * @var int
     */
    public int $file_size = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}