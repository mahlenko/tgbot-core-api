<?php

namespace tgbot\CoreAPI\Telegram\Types;

/**
 * Represents the content of a venue message to be sent as the result of an
 * inline query.
 * @package tgbot\CoreAPI\Types
 * @see https://core.telegram.org/bots/api#inputvenuemessagecontent
 */
class InputVenueMessageContent extends InputMessageContent
{
    /**
     * Latitude of the venue in degrees
     * @var float
     */
    public float $latitude = 0.0;

    /**
     * Longitude of the venue in degrees
     * @var float
     */
    public float $longitude = 0.0;

    /**
     * Name of the venue
     * @var string
     */
    public string $title = '';

    /**
     * Address of the venue
     * @var string
     */
    public string $address = '';

    /**
     * Optional. Foursquare identifier of the venue, if known
     * @var string
     */
    public string $foursquare_id = '';

    /**
     * Optional. Foursquare type of the venue, if known. (For example,
     * “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
     * @var string
     */
    public string $foursquare_type = '';
}