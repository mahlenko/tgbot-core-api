<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents changes in the status of a chat member.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#chatmemberupdated
 */
class ChatMemberUpdated extends TelegramTypesAbstract
{
    /**
     * Chat the user belongs to
     * @var Chat
     */
    public Chat $chat;

    /**
     * Performer of the action, which resulted in the change
     * @var User
     */
    public User $from;

    /**
     * Date the change was done in Unix time
     * @var int
     */
    public int $date;

    /**
     * Previous information about the chat member
     * @var ChatMember
     */
    public ChatMember $old_chat_member;

    /**
     * New information about the chat member
     * @var ChatMember
     */
    public ChatMember $new_chat_member;

    /**
     * Optional. Chat invite link, which was used by the user to join the chat; for joining by invite link events only.
     * @var ChatInviteLink
     */
    public ChatInviteLink $invite_link;


    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            Chat::class           => 'chat',
            User::class           => 'from',
            ChatMember::class     => ['old_chat_member', 'new_chat_member'],
            ChatInviteLink::class => 'invite_link'
        ];
    }
}