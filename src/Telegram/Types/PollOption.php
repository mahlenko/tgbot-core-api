<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object contains information about one answer option in a poll.
 * @see https://core.telegram.org/bots/api#polloption
 */
class PollOption extends TelegramTypesAbstract
{
    /**
     * Option text, 1-100 characters
     * @var string
     */
    public string $text = '';

    /**
     * Number of users that voted for this option
     * @var int
     */
    public int $voter_count = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}