<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Describes actions that a non-administrator user is allowed to take in a chat.
 * @see https://core.telegram.org/bots/api#chatpermissions
 */
class ChatPermissions extends TelegramTypesAbstract
{
    /**
     * Optional. True, if the user is allowed to send text messages, contacts,
     * locations and venues
     * @var bool
     */
    public bool $can_send_messages = false;

    /**
     * Optional. True, if the user is allowed to send audios, documents, photos,
     * videos, video notes and voice notes, implies can_send_messages
     * @var bool
     */
    public bool $can_send_media_messages = false;

    /**
     * Optional. True, if the user is allowed to send polls, implies can_send_messages
     * @var bool
     */
    public bool $can_send_polls = false;

    /**
     * Optional. True, if the user is allowed to send animations, games,
     * stickers and use inline bots, implies can_send_media_messages
     * @var bool
     */
    public bool $can_send_other_messages = false;

    /**
     * Optional. True, if the user is allowed to add web page previews
     * to their messages, implies can_send_media_messages
     * @var bool
     */
    public bool $can_add_web_page_previews = false;

    /**
     * Optional. True, if the user is allowed to change the chat title,
     * photo and other settings. Ignored in public supergroups
     * @var bool
     */
    public bool $can_change_info = false;

    /**
     * Optional. True, if the user is allowed to invite new users
     * to the chat
     * @var bool
     */
    public bool $can_invite_users = false;

    /**
     * Optional. True, if the user is allowed to pin messages.
     * Ignored in public supergroups
     * @var bool
     */
    public bool $can_pin_messages = false;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}