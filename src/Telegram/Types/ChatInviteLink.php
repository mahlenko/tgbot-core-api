<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Represents an invite link for a chat.
 * @package tgbot\CoreAPI\Telegram\Types
 */
class ChatInviteLink extends TelegramTypesAbstract
{
    /**
     * The invite link. If the link was created by another chat administrator,
     * then the second part of the link will be replaced with "...".
     * @var string
     */
    public string $invite_link = '';

    /**
     * Creator of the link
     * @var User
     */
    public User $creator;

    /**
     * True, if the link is primary
     * @var bool
     */
    public bool $is_primary = true;

    /**
     * True, if the link is revoked
     * @var bool
     */
    public bool $is_revoked = true;

    /**
     * Optional. Point in time (Unix timestamp) when the link will expire or has been expired
     * @var int
     */
    public int $expire_date = 0;

    /**
     * Optional. Maximum number of users that can be members of the chat
     * simultaneously after joining the chat via this invite link; 1-99999
     * @var int
     */
    public int $member_limit = 0;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            User::class => 'creator'
        ];
    }
}