<?php

namespace tgbot\CoreAPI\Telegram\Types;

use http\Exception\RuntimeException;

/**
 * This object represents the contents of a file to be uploaded. Must be posted
 * using multipart/form-data in the usual way that files are uploaded via the
 * browser.
 * @package tgbot\CoreAPI\Types
 */
class InputFile
{
    /**
     * @var mixed|null
     */
    public $url;

    /**
     * @var array
     */
    public array $file = [];

    public function __construct($file_url = null)
    {
        /*
         * Проверим наличие файла
         * */
        $parse_url = parse_url($file_url);
        if (key_exists('host', $parse_url)) {
            $this->url = $file_url;
            return;
        }

        if (! file_exists($file_url)) {
            throw new RuntimeException(sprintf('File %s not found.', $parse_url['path']));
        }

        /*
         *
         * */
        $unique = uniqid();
        $this->url = 'attach://'. $unique;
        $this->file = [
            'name' => $unique,
            'contents' => $this->getFile($file_url)
        ];
    }

    /***
     * @param string $file_url
     * @return false|string
     */
    public static function getFile(string $file_url)
    {
        return fopen($file_url, 'r');
    }
}