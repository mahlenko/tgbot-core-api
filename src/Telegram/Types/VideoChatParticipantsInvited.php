<?php


namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents a service message about new members invited to a voice chat.
 * @package tgbot\CoreAPI\Telegram\Types
 * @see https://core.telegram.org/bots/api#voicechatparticipantsinvited
 */
class VideoChatParticipantsInvited extends TelegramTypesAbstract
{
    /**
     * Optional. New members that were invited to the voice chat
     * @var User[]
     */
    public array $users = [];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            User::class => ['users:array']
        ];
    }
}