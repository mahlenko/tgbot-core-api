<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object contains information about one member of a chat.
 * @see https://core.telegram.org/bots/api#chatmember
 */
class ChatMember extends TelegramTypesAbstract
{
    /**
     * Information about the user
     * @var User
     */
    public User $user;

    /**
     * The member's status in the chat. Can be “creator”, “administrator”,
     * “member”, “restricted”, “left” or “kicked”
     * @var string
     */
    public string $status = '';

    /**
     * Optional. Owner and administrators only. Custom title for this user
     * @var string
     */
    public string $custom_title = '';

    /**
     * Optional. Owner and administrators only. True, if the user's presence in the chat is hidden
     * @var bool
     */
    public bool $is_anonymous = true;

    /**
     * Optional. Administrators only. True, if the bot is allowed to
     * edit administrator privileges of that user
     * @var bool
     */
    public bool $can_be_edited = false;

    /**
     * Optional. Administrators only. True, if the administrator can access the
     * chat event log, chat statistics, message statistics in channels,
     * see channel members, see anonymous administrators in supergroups and
     * ignore slow mode. Implied by any other administrator privilege
     * @var bool
     */
    public bool $can_manage_chat = true;

    /**
     * Optional. Administrators only. True, if the administrator can post in the
     * channel; channels only
     * @var bool
     */
    public bool $can_post_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can edit messages
     * of other users and can pin messages; channels only
     * @var bool
     */
    public bool $can_edit_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can delete
     * messages of other users
     * @var bool
     */
    public bool $can_delete_messages = false;

    /**
     * Optional. Administrators only. True, if the administrator can manage voice chats
     * @var bool
     */
    public bool $can_manage_voice_chats = true;

    /**
     * Optional. Administrators only. True, if the administrator can restrict,
     * ban or unban chat members
     * @var bool
     */
    public bool $can_restrict_members = false;

    /**
     * Optional. Administrators only. True, if the administrator can add new
     * administrators with a subset of his own privileges or demote administrators
     * that he has promoted, directly or indirectly (promoted by administrators
     * that were appointed by the user)
     * @var bool
     */
    public bool $can_promote_members = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to change the chat title, photo and other settings
     * @var bool
     */
    public bool $can_change_info = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to invite new users to the chat
     * @var bool
     */
    public bool $can_invite_users = false;

    /**
     * Optional. Administrators and restricted only. True, if the user is allowed
     * to pin messages; groups and supergroups only
     * @var bool
     */
    public bool $can_pin_messages = false;

    /**
     * Optional. Restricted only. True, if the user is a member of the chat at
     * the moment of the request
     * @var bool
     */
    public bool $is_member = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send text messages,
     * contacts, locations and venues
     * @var bool
     */
    public bool $can_send_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send audios,
     * documents, photos, videos, video notes and voice notes
     * @var bool
     */
    public bool $can_send_media_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send polls
     * @var bool
     */
    public bool $can_send_polls = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to send animations,
     * games, stickers and use inline bots
     * @var bool
     */
    public bool $can_send_other_messages = false;

    /**
     * Optional. Restricted only. True, if the user is allowed to add web page
     * previews to their messages
     * @var bool
     */
    public bool $can_add_web_page_previews = false;

    /**
     * Optional. Restricted and kicked only. Date when restrictions will be
     * lifted for this user; unix time
     * @var int
     */
    public int $until_date = 0;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            User::class => 'user'
        ];
    }
}