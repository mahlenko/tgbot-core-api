<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * This object represents the content of a message to be sent as a result of an
 * inline query. Telegram clients currently support the following 4 types:
 *
 * | InputTextMessageContent
 * | InputLocationMessageContent
 * | InputVenueMessageContent
 * | InputContactMessageContent
 *
 * @package tgbot\CoreAPI\Types
 */
class InputMessageContent extends TelegramTypesAbstract
{
    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}