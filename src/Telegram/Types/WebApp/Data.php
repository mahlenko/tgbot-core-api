<?php

namespace tgbot\CoreAPI\Telegram\Types\WebApp;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Contains data sent from a Web App to the bot.
 * @see https://core.telegram.org/bots/webapps
 * @see https://core.telegram.org/bots/api#webappdata
 */
class Data extends TelegramTypesAbstract
{
    /**
     * The data. Be aware that a bad client can send arbitrary data in this field.
     * @var string
     */
    public string $data;

    /**
     * Text of the web_app keyboard button, from which the Web App was opened.
     * Be aware that a bad client can send arbitrary data in this field.
     * @var string
     */
    public string $button_text;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}