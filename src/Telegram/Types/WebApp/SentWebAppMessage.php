<?php

namespace tgbot\CoreAPI\Telegram\Types\WebApp;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Contains information about an inline message sent by a Web App on behalf of a user.
 * @see https://core.telegram.org/bots/api#sentwebappmessage
 */
class SentWebAppMessage extends TelegramTypesAbstract
{
    /**
     * Optional. Identifier of the sent inline message.
     * Available only if there is an inline keyboard attached to the message.
     * @var string
     */
    public string $inline_message_id = '';

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}