<?php

namespace tgbot\CoreAPI\Telegram\Types\WebApp;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

/**
 * Contains information about a Web App.
 * @see https://core.telegram.org/bots/webapps
 * @see https://core.telegram.org/bots/api#webappinfo
 */
class Info extends TelegramTypesAbstract
{
    /**
     * An HTTPS URL of a Web App to be opened with additional data as specified in Initializing Web Apps
     * @var string
     */
    public string $url;

    /**
     * @return mixed
     */
    public function rules()
    {
        return [];
    }
}