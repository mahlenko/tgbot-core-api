<?php

namespace tgbot\CoreAPI\Telegram\Types;

use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;


/**
 * This object represent a user's profile pictures.
 * @see https://core.telegram.org/bots/api#userprofilephotos
 */
class UserProfilePhotos extends TelegramTypesAbstract
{
    /**
     * Total number of profile pictures the target user has
     * @var int
     */
    public int $total_count = 0;

    /**
     * Requested profile pictures (in up to 4 sizes each)
     * @var PhotoSize[]
     */
    public array $photos = [];

    /**
     * @return mixed
     */
    public function rules()
    {
        return [
            PhotoSize::class => 'photos:array'
        ];
    }
}