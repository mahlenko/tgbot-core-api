<?php

namespace tgbot\CoreAPI\Providers;

use Illuminate\Support\ServiceProvider;
use tgbot\CoreAPI\BotClient;

class BotServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config.php' => config_path('telegram.php'),
        ]);

        $this->app->singleton(BotClient::class, function() {
            return new BotClient(config('telegram.token'));
        });
    }
}