<?php

namespace tgbot\CoreAPI\Abstracts;

use Exception;

/**
 * @package tgbot\TelegramApi
 */
abstract class TelegramTypesAbstract
{
    /**
     * @param $data
     */
    public function __construct($data = null)
    {
        try {
            $this->fillObjectData($data);
        } catch (Exception $exception) {}
    }

    /**
     * @param $data
     */
    protected function fillObjectData($data = null)
    {
        /* object fill rules */
        $rules = $this->getRules();

        if ($data) {
            foreach ((array) $data as $key => $value) {
                /* filling */
                if (key_exists($key, $rules)) {
                    /* this data array */
                    if ($rules[$key]['is_array']) {
                        foreach ($value as $k => $v) {
                            $value[$k] = new $rules[$key]['class']($v);
                        }
                    } else {
                        $value = new $rules[$key]['class']($value);
                    }
                }

                /* default data */
                if (property_exists($this, $key)) {
                    $this->$key = $value;
                }
            }
        }

        /*  */
        $data_keys = array_keys((array) $data);
        foreach ($this as $key => $value) {
            if (array_search($key, $data_keys) === false) {
                unset($this->$key);
            }
        }
    }

    /**
     * @return array
     */
    private function getRules(): array
    {
        $rules = [];
        foreach ($this->rules() as $class => $keys)
        {
            if (!is_array($keys)) {
                $keys = [$keys];
            }

            foreach ($keys as $key)
            {
                $var = $key;
                $is_array = false;

                if ($offset = strpos($key, ':')) {
                    $is_array = true;
                    $var = substr($key, 0, $offset);
                }

                $rules[$var] = ['class' => $class, 'is_array' => $is_array];
            }
        }

        return $rules;
    }

    /**
     * @return mixed
     */
    abstract public function rules();
}