<?php

namespace tgbot\CoreAPI\Abstracts;

use Exception;
use tgbot\CoreAPI\Telegram\Types\InputFile;

/**
 * @package tgbot\TelegramApi
 */
abstract class TelegramMethodsAbstract
{
    /**
     * @var array
     */
    protected array $local_files = [];

    /**
     * Method constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @throws Exception
     */
    public function requireFields()
    {
        foreach ($this->requiredFields() as $key) {
            if (empty($this->$key)) {
                throw new Exception("You didn't fill in the required field: " . $key);
            }
        }
    }

    /**
     * @return array
     */
    public function hasFiles() {
        return $this->local_files;
    }

    /**
     * @param string $class_type
     * @param $data
     * @return array
     */
    protected function bindToObjectArray(string $class_type, $data)
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = new $class_type($item);
        }

        return $result;
    }

    /**
     * Append local file and return attach:// url
     * @param string|InputFile $filepath
     * @return string
     */
    protected function attachFile($filepath)
    {
        if (! $filepath instanceof InputFile) {
//            $filepath = new InputFile($filepath);
            return $filepath;
        }

        if ($filepath->file) {
            $this->local_files[] = $filepath->file;
        }

        return $filepath->url;
    }

    /**
     * Request fields
     * @return array
     */
    abstract public function requiredFields() : array;

    /**
     * @param $data
     * @return mixed
     */
    abstract public function bindToObject($data);

    /**
     * @return void
     */
    abstract public function beforeSending();
}