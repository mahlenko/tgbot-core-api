<?php


namespace tgbot\CoreAPI;


use tgbot\CoreAPI\Abstracts\TelegramTypesAbstract;

class TelegramRequest extends TelegramTypesAbstract
{
    /**
     * @var string
     */
    public string $token;

    /**
     * @var string
     */
    public string $method;

    /**
     * @var string
     */
    public string $url;

    /**
     * @var array
     */
    public array $params = [];

    /**
     * @var string
     */
    public string $response;

    /**
     * @return mixed|void
     */
    public function rules()
    {
        return [];
    }
}